/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { localImage } from '../../config/global';
import { DeviceHeight, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks'

export const Header = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <View>

<View style={{paddingTop: 15,paddingBottom: 15, backgroundColor: '#6600ff',marginbottom:scale(15) }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 0, flexDirection: 'row', alignItems: 'center' }}>

          <View style={{ flex: 0.9,justifyContent: 'center', marginLeft: scale(20) }}>
        
            <Text style={{
              color: 'white',
              fontSize: 20,
              fontWeight: 'bold',
            }}>{props.name} </Text>
          </View>
          <View style={{ flex: 0.1 }}>
            <TouchableOpacity
              // onPress={() => props.navigation.navigate('Notification')}
            >
              <FontAwesome name="bell" size={25} color={'white'} />
            </TouchableOpacity>
          </View>
         
        </View>
      </View>
    </View>

  );
}

export const HeaderWithBack = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <View>

<View style={{paddingTop: 15,paddingBottom: 15, backgroundColor: '#6600ff',marginbottom:scale(15) }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 0, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>

            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
              <FontAwesome name="chevron-left" size={20} color={'white'} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.8, justifyContent: 'center', }}>
            <Text style={{
              color: 'white',
              fontSize: 20,
              fontWeight: '700',
            }}>{props.name} </Text>


          </View>
        </View>
      </View>

    </View >
  );
}

export const HeaderWithBackLogo = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <View>

<View style={{paddingTop: 15,paddingBottom: 15, backgroundColor: '#6600ff',marginbottom:scale(15) }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 0, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>

            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
              <FontAwesome name="chevron-left" size={20} color={'white'} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.8, justifyContent: 'center', }}>
            <Image
              style={{ height: 25, width: 150, }}
              // resizeMode="stretch"
              source={localImage.logo}
            />

          </View>
        </View>
      </View>

    </View >
  );
}



export const HeaderDrawer = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <View>

      <View style={{paddingTop: 15,paddingBottom: 15, backgroundColor: '#6600ff',marginbottom:scale(15) }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 0, flexDirection: 'row', alignItems: 'center' }}>

          <View style={{ flex: 0.9, justifyContent: 'center', marginLeft: scale(20) }}>
            <Text style={{
              color: 'white',
              fontSize: 20,
              fontWeight: '700',
            }}>{props.name} </Text>


          </View>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>

            <TouchableOpacity
              onPress={() => props.navigation.toggleDrawer()}
              style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
              <Entypo name="dots-three-vertical" size={20} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </View >
  );
}


export const HeaderDrawerBack = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <View>

      <View style={{paddingTop: 15,paddingBottom: 15, backgroundColor: '#6600ff',marginbottom:scale(15) }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 0, flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ flex: 0.1, alignSelf: 'center' }}>

<TouchableOpacity
  onPress={() => props.navigation.goBack()}
  style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
  <FontAwesome name="chevron-left" size={20} color={'white'} />
</TouchableOpacity>
</View>
          <View style={{ flex: 0.8, justifyContent: 'center', marginLeft: scale(20) }}>
            <Text style={{
              color: 'white',
              fontSize: 20,
              fontWeight: '700',
            }}>{props.name} </Text>


          </View>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>

            <TouchableOpacity
              onPress={() => props.navigation.toggleDrawer()}
              style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
              <Entypo name="dots-three-vertical" size={20} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </View >
  );
}

