/* eslint-disable no-undef */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React from 'react';
import { Image, ImageBackground, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { numberWithCommas } from '../../util/util';
import { moderateScale, scale, verticalScale } from '../Scale';

const Card = props => {
  const { name, image, bio } = props.user;
  return (
    <View style={styles.card}>
      <ImageBackground
        source={{
          uri: image,
        }}
        style={styles.image}>
        <View style={styles.cardInner}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.bio}>{bio}</Text>
        </View>
      </ImageBackground>
    </View>
  );
};
export const Card1 = (props) => {

  const { isDisable = true, bgcolor } = props;
  return (
    <TouchableOpacity
      style={{
        height: scale(92),
        backgroundColor: props.bgcolor,
        borderRadius: 8,
        paddingLeft: 5,
        justifyContent: 'center',
        borderWidth:0.5,
        borderColor:props.border||props.bgcolor
      }}
      onPress={props.nextScreen}
      disabled={isDisable}>

      <View style={{
        marginLeft: scale(15),
      }}>
        {props.icon ? (
          <Icon
            name={props.icon}
            color={props.textcolor}
            size={25}
          />
        ) : (
          <Image
            style={{ width: scale(23.27), height: scale(23.34), tintColor: props.textcolor }}
            source={props.image}
            resizeMode="contain"
          />
        )}
      </View>




      <Text
        style={{
          color: props.textcolor,
          fontSize: scale(14),
          marginLeft: scale(15),
          marginTop: verticalScale(10),
          fontWeight:'600'
        }}>
        {props.card_name}
      </Text>
    </TouchableOpacity>
  );
}
export const Empty_Card = (props) => {

  return (
    <View
      style={{
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        margin :15,
      }}>
      {/* <Text
              style={{ fontSize: scale(20), textAlign: 'center' }}
              numberOfLines={2}>
              {props.isYou ? 'Looks like' : 'Looks like your'}
          </Text> */}
      
      <Image
        style={{
          width: scale(260),
          height: scale(171),
          marginTop: verticalScale(11),
        }}
        resizeMode="cover"
        source={require('../../assets/empty.png')}
      />
      <Text
        style={{ fontSize: scale(20),marginTop:scale(16) , textAlign: 'center' }}
        numberOfLines={2}>
        {props.Text}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
    backgroundColor: '#fefefe',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
    overflow: 'hidden',

    justifyContent: 'flex-end',
  },
  cardInner: {
    padding: 10,
  },
  name: {
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
  },
  bio: {
    fontSize: 18,
    color: 'white',
    lineHeight: 25,
  },
});

export default Card;