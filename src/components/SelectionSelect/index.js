/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Animated,
  Button,
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Colors } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient';

export const GetIcon = (props) => {
  const { name, IconColor } = props
  if (name == 'Audio') {
    return <View style={{ borderRadius: 30, width: 50, height: 50, backgroundColor: '#4E4B4B', alignItems: 'center', justifyContent: 'center' }}>
      <MaterialCommunityIcons name="microphone" size={35} color={IconColor} />
    </View>

  } else if (name == 'Image') {
    return <View style={{ borderRadius: 30, width: 50, height: 50, backgroundColor: '#4E4B4B', alignItems: 'center', justifyContent: 'center' }}>

      <MaterialCommunityIcons name="image" size={30} color={IconColor} />
    </View>


  } else if (name == 'Video') {
    return <View style={{ borderRadius: 30, width: 50, height: 50, backgroundColor: '#4E4B4B', alignItems: 'center', justifyContent: 'center' }}>

      <FontAwesome name="video-camera" size={25} color={IconColor} />
    </View>


  } else {
    return <View style={{ borderRadius: 30, width: 50, height: 50, backgroundColor: '#4E4B4B', alignItems: 'center', justifyContent: 'center' }}>
      <FontAwesome name="telegram" size={35} color={IconColor} />
    </View>


  }
}

const SelectionSelect = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        marginTop: verticalScale(15),
        marginLeft: scale(5),
      }}>
      {props.button.selected ? (
        <View
          style={{
            borderWidth: 1,
            paddingLeft: 7,
            paddingRight: 7,
            borderColor: props.selectedTextColor || Colors.green,
            backgroundColor: props.selectedTextColor || Colors.green,
            borderRadius: scale(5),
            //marginLeft:scale(10)
            //this.props.button.label === 'Purchase Orders' ? 0 : scale(10),
          }}
        >
          <Text
            style={{
              color: props.unSelectedTextColor || Colors.yellow3B,
              fontSize: props.fontSize || scale(18),
            }}>
            {props.button.label}
          </Text>
        </View>
      ) : (
        <View
          style={{
            borderWidth: 1,
            paddingLeft: 7,
            paddingRight: 7,
            borderColor: props.selectedTextColor || Colors.whiteFF,
            backgroundColor: props.unSelectedTextColor || Colors.red00,
            borderRadius: scale(5),
          }}>
          <Text
            style={{
              color: props.selectedTextColor || Colors.red00,
              fontSize: props.fontSize || scale(18),
            }}>
            {props.button.label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );

}
export default SelectionSelect;
export const SelectionSelect1 = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        
        marginLeft: scale(10),
        padding: 3,
      }}>
      {props.button.selected ? (
        <View
          style={{
            //marginLeft:scale(10)
            //this.props.button.label === 'Purchase Orders' ? 0 : scale(10),
            borderBottomWidth:4,
            borderColor: props.selectedTextColor || Colors.red00
          }}
        >
          <Text
            style={{
              color: props.selectedTextColor || Colors.red00,
              fontSize: props.fontSize || scale(14),
              fontWeight: 'bold'
            }}>
            {props.button.label}
          </Text>

        </View>
      ) : (
        <View
          style={{
          
          }}>
          <Text
            style={{
              color: colors.text,
              fontSize: props.fontSize || scale(14),
            }}>
            {props.button.label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );

}

export const SelectionSelectTab = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        marginTop: verticalScale(15),
        flex: 1,
        backgroundColor: props.button.selected ? props.selectedBackgraoundColor : 'gray',
        // borderWidth:1,
        padding: 10,

      }}>
      {props.button.selected ? (

        <Text
          style={{
            color: colors.black0,
            fontSize: props.fontSize || scale(18),
            fontWeight: '800',
            marginLeft: 10
          }}>
          {props.button.label}
        </Text>
      ) : (

        <Text
          style={{
            color: Colors.whiteFF,
            fontSize: props.fontSize || scale(18),
            marginLeft: 10

          }}>
          {props.button.label}
        </Text>
      )}
    </TouchableOpacity>
  );

}
export const SelectionSelectTabIcon = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        marginTop: verticalScale(15),
        flex: 1,
        // backgroundColor:props.button.selected?props.selectedBackgraoundColor :'gray',
        // borderWidth:1,

        justifyContent: 'center',

      }}>
      {props.button.selected ? (
        <GetIcon name={props.button.label} IconColor={'#1098FC'} />

      ) : (
        <View>
          <GetIcon name={props.button.label} IconColor={Colors.whiteFF} />
        </View>
      )}
    </TouchableOpacity>
  );

}

export const SelectionSelectOption = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
     >
      {props.button.selected ? (
        <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#0038F5', '#9F03FF']}
      
        style={{
          borderRadius: scale(5),
          // height:scale(40),
          // width:scale(40),
          // alignItems:'center',
          // justifyContent:'center'
          padding: 10,
          
        }}>
          <Text
            style={{
              color: props.selectedTextColor || Colors.red00,
              fontSize: props.fontSize || scale(14),
              fontWeight: 'bold'
            }}>
            {props.button.label}
          </Text>

          </LinearGradient>
      ) : (
        <View
          style={{
        padding: 10,
          }}>
          <Text
            style={{
              color: colors.text,
              fontSize: props.fontSize || scale(14),
            }}>
            {props.button.label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );

}