/* eslint-disable sort-keys */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */

import React, { useEffect, useState, useRef } from 'react';
import { FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { DeviceHeight, DeviceWidth, imageslist, localImage } from '../../config/global'
import { scale } from '../Scale';
import { CountdownTimer } from '../Common/CountdownTimer';
import { ImageComponent, VideoComponent } from '../Common/heper'
export const TopSellerComponent = (props) => {
    const [imageLoading, setIsImageLoading] = useState(true);

    const { item } = props;
    const { itemDetails = {} } = item;
    return (
        <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
            <View style={{ flexDirection: 'row', padding: 10 }}>
                <View style={{ flex: 0.15, flexDirection: 'row', }}>
                    {item.fileType ? item.fileType === 'IMAGE' ? (
                        <ImageComponent
                            url={itemDetails.imageURL}
                            imageStyle={{ width: scale(30), height: scale(30), borderRadius: 10 }}
                        />
                    ) : <VideoComponent
                        url={itemDetails.imageURL}
                        imageStyle={{ width: scale(30), height: scale(30), borderRadius: 10, backgroundColor: 'black', }}
                    /> : null}

                </View>
                <View style={{ flex: 0.85, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 14 }}>{itemDetails.name}</Text>
                    <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{item.currentPrice + ' ETH'}</Text>
                </View>
            </View>
        </View>
    );
};

export const CategoryList = (props) => {
    const { item } = props;
    return (<View style={{ borderWidth: 1, borderColor: 'white' }}>
        <TouchableOpacity >
            <Image
                style={{
                    width: scale(50), height: scale(50)
                }}
                source={item.categoryName === 'art' ? imageslist[0].image : item.categoryName === 'Digital' ? imageslist[1].image : imageslist[2].image}
            />
            <View style={{ flexDirection: 'row', backgroundColor: 'red', justifyContent: 'center' }}>

                <Text style={{ textAlign: 'center', color: 'white', padding: 7 }}>{item.categoryName.toUpperCase()}</Text>
            </View>
        </TouchableOpacity>
    </View>
    );
};
export const TrendingList = (props) => {
    const { item = {} } = props;
    const { itemDetails = {} } = item;
    const now = Date.now();
    const t1 = (new Date(item.auctionEndTimestamp)).getTime()

    return (t1>now?<View style={{ marginRight: scale(10), padding: 10,marginBottom:30, borderRadius: scale(10), backgroundColor: '#FFFFFF', borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
        <TouchableOpacity onPress={() => props.onChange(item)}>
            <View style={{ alignItems: 'center', justifyContent: 'center', height: 270, width: DeviceWidth / 2 }}>




                {item.fileType ? item.fileType === 'IMAGE' ? (
                    <ImageComponent
                        url={itemDetails.imageURL}
                        imageStyle={{ width: DeviceWidth / 2, height: 260, borderRadius: 10 }}
                    />
                ) : <VideoComponent
                    url={itemDetails.imageURL}
                    imageStyle={{ width: DeviceWidth / 2, height: 260, borderRadius: 10, backgroundColor: 'black', }}
                /> : null}

                {/* {itemDetails.imageURL ? <Image style={{ width: DeviceWidth / 2, height: 260, borderRadius: 10 }} source={{ uri: itemDetails.imageURL }} onLoadStart={() =>setLoadImage(true)} onLoadEnd={() =>setLoadImage(false)}/> : null} */}

                {item.auctionEndTimestamp ? <View style={{
                    position: 'absolute',
                    right: 5,
                    top: 5,
                }}>
                    <CountdownTimer targetDate={item.auctionEndTimestamp} />
                </View> : null}
            </View>

            <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontSize: 14, fontWeight: 'bold', }}>{itemDetails.name}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 0.8, flexDirection: 'row' }}>
                    <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="ethereum" size={26} color={'#EE76AD'} />
                    <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + ' ETH'}</Text>
                </View>
                <View style={{ flex: 0.3, justifyContent: 'center', flexDirection: 'row' }}>
                    <AntDesignIcon style={{ alignSelf: 'center' }} name="hearto" size={18} color={'#DADADA'} />
                    <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: 'gray' }}>{item.favCount + ''}</Text>
                </View>
            </View>

        </TouchableOpacity>
    </View>:null
    );
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
    },
    mediaPlayer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
});