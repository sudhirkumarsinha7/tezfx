/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from "../Scale";
// export const PopUp2 = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(250), height: scale(250), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
//                 <Image
//                     style={{ width: scale(46.16), height: scale(46.16), }}
//                     source={props.image}
//                     resizeMode='contain'
//                 />


//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(20), marginBottom: verticalScale(20), color: "#707070", marginRight: scale(27), marginLeft: scale(27) }}>{props.text1}</Text>

//                 <View style={{ flexDirection: 'row' }}>
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             // backgroundColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.cancel}>
//                         <Text style={{ color: Colors.blueFc, fontSize: scale(15) }}>{props.label1}</Text>
//                     </TouchableOpacity>
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             marginLeft: scale(10),
//                             padding: 10,
//                             borderRadius: 10,
//                             backgroundColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.Ok}>
//                         <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
//                     </TouchableOpacity>
//                 </View>

//             </View>
//         </Modal>
//     )
// }

// export const PopUp1 = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(300), height: scale(200), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>

//                 <Image
//                     style={{ width: scale(30.16), height: scale(30.16), tintColor: Colors.blueFc }}
//                     source={props.image}
//                     resizeMode='contain'
//                 />

//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(10), color: Colors.blueFc, marginRight: scale(15), marginLeft: scale(15), fontWeight: 'bold' }}>{props.text2}</Text>

//                 <View style={{ flexDirection: 'row', marginTop: verticalScale(20), justifyContent: 'space-between' }}>
//                     {/* <View style={{ flex: 0.4, }}> */}
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             borderColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.cancel}>

//                         <Text style={{ color: Colors.backgroundBlue, fontSize: scale(12) }}>{props.label1}</Text>

//                     </TouchableOpacity>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             // borderRadius: 10,
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             backgroundColor: Colors.yellow3B,
//                             marginLeft: 10

//                         }}
//                         onPress={props.Ok}>

//                         <Text style={{ color: '#FFFFFF', fontSize: scale(12) }}>{props.label}</Text>
//                     </TouchableOpacity>
//                     {/* </View> */}
//                 </View>
//             </View>
//         </Modal>
//     );
// }

// export const Logout_PopUp = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(250), height: scale(220), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>

//                 <Text style={{ fontSize: scale(20), textAlign: 'center', }}>Log Out</Text>

//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(50), color: "#707070", marginRight: scale(20), marginLeft: scale(20) }}>Are you sure you want to logout?</Text>

//                 <View style={{ width: scale(230), flexDirection: "row", marginTop: verticalScale(50), alignItems: "center", justifyContent: "space-between" }}>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             borderRadius: 10,
//                             width: scale(105),
//                             height: scale(35),
//                         }}
//                         onPress={props.Cancel}>
//                         <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>Cancel</Text>

//                     </TouchableOpacity>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             borderRadius: 10,
//                             width: scale(105),
//                             borderColor: Colors.blueFc,
//                             backgroundColor: "#FFFFFF",
//                             borderWidth: 1,
//                             height: scale(35),
//                         }}
//                         onPress={props.Logout}>

//                         <Text style={{ color: '#0093E9', fontSize: scale(15) }}>Log out</Text>

//                     </TouchableOpacity>

//                 </View>
//             </View>
//         </Modal >
//     )
// }

export const PopUp1 = (props) => {

    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: DeviceWidth / 1.2, height: scale(250), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                <Image
                    style={{ width: scale(46.16), height: scale(46.16), }}
                    source={props.image}
                    resizeMode='contain'
                />


                <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(20), marginBottom: verticalScale(20), color: "#707070", marginRight: scale(27), marginLeft: scale(27) }}>{props.text1}</Text>


                <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: DeviceWidth / 1.4,
                        padding: 10,
                        borderRadius: 10,
                        backgroundColor: Colors.blueFc,
                    }}
                    onPress={props.Ok}>
                    <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                </TouchableOpacity>


            </View>
        </Modal>
    )
}
export const PopUp = (props) => {
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: DeviceWidth / 1.2, height: scale(300), backgroundColor: "#ffffff", borderRadius: 20,  }}>
            <View style={{flexDirection:'row',justifyContent:'center',margin:scale(15)}}>

                <Image
                    style={{ width: scale(35.16), height: scale(35.16), }}
                    source={props.image}
                    resizeMode='contain'
                />
{/* <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        padding: 10,
                        borderRadius: 10,
                    }}
                    onPress={props.cancel}>
                    <Icon name="close-circle" size={35} color={Colors.red00} />
                </TouchableOpacity> */}
                </View>
                
                <Text style={{ fontSize: scale(15),marginLeft: scale(15),fontWeight:'600' }}>{'Payment Successful !'}</Text>
                <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(15),fontWeight:'400' }}>{'Your crypto has been deposited in'}</Text>
                <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(15),fontWeight:'400' }}>{'Wallet ID'}</Text>
                <Text style={{ fontSize: scale(12), marginTop: verticalScale(10), marginLeft: scale(15),fontWeight:'400',color:'#0079E8' }}>{'0xC5B0E3C2e90...6B248Ab012323Ae53F082f6'}</Text>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: DeviceWidth / 1.4,
                            padding: 10,
                            borderRadius: 10,
                            backgroundColor: '#4F46E5',
                            marginTop: 40
                        }}
                        onPress={props.Ok}>
                        <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    )
}
export const PopUp2 = (props) => {
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: DeviceWidth / 1.2, height: scale(250), backgroundColor: "#ffffff", borderRadius: 20,  }}>
                <View style={{flexDirection:'row',justifyContent:'center',margin:scale(15)}}>
                <Image
                    style={{ width: scale(35.16), height: scale(35.16), }}
                    source={props.image}
                    resizeMode='contain'
                />
{/* <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        padding: 10,
                        borderRadius: 10,
                    }}
                    onPress={props.cancel}>
                    <Icon name="close-circle" size={35} color={Colors.red00} />
                </TouchableOpacity> */}
                </View>
                
                <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(15),fontWeight:'400',marginRight: scale(15) }}>{props.text1}</Text>
                
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: DeviceWidth / 1.4,
                            padding: 10,
                            borderRadius: 10,
                            backgroundColor: '#4F46E5',
                            marginTop: 40
                        }}
                        onPress={props.Ok}>
                        <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    )
}

