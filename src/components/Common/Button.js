import React, { useEffect, useState, useRef } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';

import { Colors, DeviceWidth, CommonStyle } from '../Common/Style';
import { moderateScale, scale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import { join } from 'path';

export const CustomButton = (props) => {

  return (
    <TouchableOpacity
      style={{
        height: scale(70),
        borderRadius: 5,
        width: '30%',
        backgroundColor: props.backgroundColor || '#0093E9',
        margin: verticalScale(5),
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={props.Press}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Image
          style={{
            width: props.imgWidth || scale(18.73),
            height: props.imgHight || scale(18.73),
          }}
          source={props.img}
        />
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: scale(10),
          marginTop: scale(5)
        }}>
        <Text
          style={{
            fontSize: scale(13),
            color: '#FFFFFF',
            fontWeight: 'bold',
          }}>
          {props.label1}
        </Text>
        <Text
          style={{
            fontSize: scale(13),
            color: '#FFFFFF',
            fontWeight: 'bold',
          }}>
          {props.label2}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
export const CustomButton1 = (props) => {
  const { enable = true } = props
  return (
    <TouchableOpacity
      style={{
        // width: DeviceWidth/1.1,
        // height: scale(45),
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 5,
        backgroundColor:enable? props.backgroundColor || '#0093E9':'gray',
        flexDirection: 'row',
        marginLeft: verticalScale(10),
        marginRight: verticalScale(10),
        margin: scale(5),
        alignItems: 'center',
        justifyContent: 'center',
      }}
      disabled={enable ? false : true}
      onPress={props.Press}>
      {props.img ? <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Image
          style={{
            width: props.imgWidth || scale(18.73),
            height: props.imgHight || scale(18.73),
          }}
          source={props.img}
        />
      </View> : null}
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: scale(10),
        }}>
        <Text
          style={{
            fontSize: scale(15),
            color: '#FFFFFF',
            fontWeight: 'bold',
            textAlign: 'center'
          }}>
          {props.label1}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
export const CustomLinearBotton =(props)=>{
    return  <LinearGradient
    start={{ x: 0, y: 0 }}
    end={{ x: 1, y: 0 }}
    colors={['#0038F5', '#9F03FF']}

    style={{
      borderRadius: scale(10),
      padding: scale(13),
      margin:scale(10)

    }}>
        <TouchableOpacity  onPress={props.onNavigate}>
        <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
    </TouchableOpacity>
    </LinearGradient>
}
export const CustomLinearBottonCircle =(props)=>{
  return  <LinearGradient
  start={{ x: 0, y: 0 }}
  end={{ x: 1, y: 0 }}
  colors={['#0038F5', '#9F03FF']}

  style={{
    borderRadius: scale(20),
    height:scale(40),
    width:scale(40),
    alignItems:'center',
    justifyContent:'center'
    
    
  }}>
      <TouchableOpacity  onPress={props.onNavigate} disabled={true}>
      <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
  </TouchableOpacity>
  </LinearGradient>
}
export const CustomLinearBotton1 =(props)=>{
    return  <LinearGradient
    start={{ x: 0.0, y: 1.0 }} 
    end={{ x: 1.0, y: 1.0 }}
    colors={['#0038F5', '#9F03FF']}

    style={{
      borderRadius: scale(10),
      padding: scale(2),
      margin:scale(10)

    }}>
        <TouchableOpacity  onPress={props.onNavigate} style={{
        backgroundColor: Colors.black0,
        borderRadius: scale(10),
        padding:scale(10)
        }}>
        <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
    </TouchableOpacity>
    </LinearGradient>
}

export const CustomLinearBotton2 =(props)=>{
  return  <LinearGradient
  start={{ x: 0, y: 0 }}
  end={{ x: 1, y: 0 }}
  colors={props.isColorful?['#0038F5', '#9F03FF']:['gray', 'gray']}

  style={{
    borderRadius: scale(20),
   padding:5,
    margin:scale(10)

  }}>
      <TouchableOpacity  onPress={props.onNavigate}>
      <Text style={{color:Colors.whiteFF,fontSize:scale(14),textAlign:'center',fontWeight:'bold', paddingLeft: scale(10),paddingRight: scale(10)}}>{props.title}</Text>
  </TouchableOpacity>
  </LinearGradient>
}