import {
  Dimensions,
} from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;
export const UserProf = {
  name: 'John Doe',
  company: 'Far N Par (India) Private Limited',
  pdaAmount: '27,76,000'
}

export const profileData = {
  firstName: {
    displayName: 'First Name',
    stateName: 'firstName',
    type: 'String',
    required: true,
  },
  lastName: {
    displayName: 'Last Name',
    stateName: 'lastName',
    type: 'String',
    required: true,
  },
  phoneNumber: {
    displayName: 'Phone',
    stateName: 'phoneNumber',
    type: 'Stirng',
    required: true,
  },
  location: {
    displayName: 'Location',
    stateName: 'location',
    type: 'Stirng',
    required: true,
  },
  role: {
    displayName: 'Role',
    stateName: 'role',
    type: 'Stirng',
    required: true,
  },
  emailId: {
    displayName: 'Email ID',
    stateName: 'emailId',
    type: 'Stirng',
    required: true,
  },
};

export const CreateNftData = {
  _assetname: {
    displayName: 'Name *',
    stateName: '_assetname',
    type: 'String',
    required: true,
  },
  _externallink: {
    displayName: 'External Links',
    stateName: '_externallink',
    type: 'String',
    required: true,
  },
  _assetdesc: {
    displayName: 'Decscription',
    stateName: '_assetdesc',
    type: 'Stirng',
    required: true,
  },
  price: {
    displayName: 'Price (ETH)',
    stateName: 'price',
    type: 'Stirng',
    required: true,
  },
  royalty: {
    displayName: 'Royalty',
    stateName: 'royalty',
    type: 'Stirng',
    required: true,
  },
  duration: {
    displayName: 'Duration (days)',
    stateName: 'duration',
    type: 'Stirng',
    required: true,
  },

  category: {
    displayName: 'Category',
    stateName: 'category',
    type: 'Stirng',
    required: true,
  },
  collection: {
    displayName: 'Collection',
    stateName: 'collection',
    type: 'Stirng',
    required: true,
  },
};
