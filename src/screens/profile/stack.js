import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Login from '../Login';
import Register from '../Register';
import forgotPass from '../Login/forgotPass';
import Kyc from './kyc';
import AccountInfo from './accountInfo';


import Profile from './index';
const Stack = createNativeStackNavigator();
const ProfileStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="forgotPass"
        component={forgotPass}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{
          headerShown: false,
        }}
      />
 <Stack.Screen
        name="accountInfo"
        component={AccountInfo}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Kyc"
        component={Kyc}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default ProfileStack;
