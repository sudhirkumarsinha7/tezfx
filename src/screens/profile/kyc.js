import React, { useState, useEffect } from "react";
import { ImageBackground, View, Text, StyleSheet, TouchableOpacity, Image, TextInput,Keyboard,Button,TouchableHighlight } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { DeviceWidth } from "../../config/global";
import { scale } from '../../components/Scale';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';
import { InputTextHelperNew, DropdownView } from '../../components/Common/heper'
import { HeaderWithBackLogo } from '../../components/Header'
import {CustomButton1}  from '../../components/Common/Button'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {LogIn} from '../../redux/action/auth'
import { connect } from 'react-redux';
import {config, getIosDeviceTokeApi} from '../../config/config';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
import axios from "axios";
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
const SUMSUB_APP_TOKEN='sbx:ifys5XlMycS6BDZKBU37LZYU.nehuy0r8rVevl6vQhZLOrWTDAoHV3lGg'
const SUMSUB_SECRET_KEY='UCmOdiXXDzxkLPszn9GKJusImYGd12wL'
import RazorpayCheckout from 'react-native-razorpay';

const KycScreen = (props) => {
  const isFocused = useIsFocused();
  const [isMandatory, setMandatory] = useState(false)
  const [error, setErrorMessage] = useState('')
  const [isLoader, setLoader] = useState(false)
  useEffect(() => {

  }, [isFocused]);

  let launchSNSMobileSDK = () => {

    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    //
    let accessToken ='_act-sbx-211520d8-bf23-4e1a-a6ea-9d268e1e194a';
  
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
        let data={
            "id": userInfo.email
        }
        // this is a token expiration handler, will be called if the provided token is invalid or got expired
        // call your backend to fetch a new access token (this is just an example)
        return fetch(config().createApplicant, {
          method: 'POST',
          body: JSON.stringify(data),
        }).then(resp => {
console.log('resp ',resp)
          // return a fresh token from here
          return accessToken
        })
      })
      .withHandlers({ // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: (event) => {
          console.log("onStatusChanged: [" + event.prevStatus + "] => [" + event.newStatus + "]");
        },
        onLog: (event) => {
          console.log("onLog: [Idensic] " + event.message);
        }
      })
      .withDebug(true)
      .withLocale('en') // Optional, for cases when you need to override the system locale
      .build();
  
    snsMobileSDK.launch().then(result => {
      console.log("SumSub SDK State: " + JSON.stringify(result))
    }).catch(err => {
      console.log("SumSub SDK Error: " + JSON.stringify(err))
    });
  }

  return (<View >
    <HeaderWithBackLogo
      navigation={props.navigation}
      name={'Kyc'}
    />
    <View style={{ margin: scale(15) }}>
    {/* <Button onPress={launchSNSMobileSDK} title="Launch SumSub SDK" /> */}
    <TouchableHighlight onPress={() => {
  var options = {
    description: 'Credits towards consultation',
    image: 'https://i.imgur.com/3g7nmJC.png',
    currency: 'INR',
    key: 'rzp_test_mAOr5LeoRJXmSz', // Your api key
    amount: '5000',
    name: 'foo',
    prefill: {
      email: 'void@razorpay.com',
      contact: '9191919191',
      name: 'Razorpay Software'
    },
    theme: {color: 'green'}
  }
  RazorpayCheckout.open(options).then((data) => {
    // handle success
    alert(`Success: ${data.razorpay_payment_id}`);
  }).catch((error) => {
    // handle failure
    alert(`Error: ${error.code} | ${error.description}`);
  });
}}>
    <Text>pay</Text>
    </TouchableHighlight>

    </View>
  </View>)

}
const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
  },

});

function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  LogIn
})(KycScreen);
