/* eslint-disable react-native/no-inline-styles */



import { HeaderWithBack } from '../../components/Header'
import { View, Text, TouchableOpacity,Image } from 'react-native';
import { URL } from '../../config/config';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { localImage, imageslist, DeviceWidth } from '../../config/global';

const AccountInfo = (props) => {
    const { userInfo = {} } = props

    return <View  style={{backgroundColor:'white'}}>
        <HeaderWithBack
            navigation={props.navigation}
            name={'Profile'}
        />

        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderColor: '#C8C8C8',marginTop:scale(20) }}>
        <View  style={{ height: 100, width: 100,borderWidth:2,backgroundColor:'#6600ff',borderRadius:64,borderColor:'white',marginBottom:scale(15) }}>
        <Image
        source={localImage.account}
        style={{height: 90, width: 100,borderRadius:64,alignItems:'center'}}
        />
        </View>
            <View style={{ justifyContent: 'center', marginLeft: scale(20), alignItems: 'center' }}>
                <Text style={{ fontSize: scale(18), fontWeight: '700', color: '#1D3A70' }}>{userInfo.token ? userInfo.firstName + ' ' + userInfo.lastName : 'Guest'}</Text>
                <Text style={{ fontSize: scale(16), fontWeight: '400', marginTop: scale(10) }}>{userInfo.token ? userInfo.email : ''}</Text>
            </View>
        </View>

        <View style={{ marginTop: scale(15), marginLeft: scale(15), marginRight: scale(15) }}>
            <View style={{ flexDirection: 'row', }}>
                <AntDesign name="lock1" size={18} color={'#FFB9AA'} />

                <Text style={{ fontSize: scale(16), fontWeight: '600', marginBottom: scale(20), color: '#1D3A70', alignSelf: 'center', marginLeft: scale(20) }}>{'Your API Access Key'}</Text>

            </View>
            <Text style={{ fontSize: scale(14), fontWeight: '400', color: '#555C6B', textAlign: 'justify' }}>{'Your API access key is your unique authentication token used to gain access to the Statwig API.'}</Text>
            <View style={{ backgroundColor: 'black', padding: scale(10), marginTop: scale(10), marginBottom: scale(10), borderRadius: scale(5) }}>


                <Text style={{ fontSize: scale(12), fontWeight: '400', color: 'white', textAlign: 'justify' }}>{URL + '/client/submitPayment'}</Text>
            </View>
            <Text style={{ fontSize: scale(14), fontWeight: '400', color: '#555C6B', textAlign: 'justify' }}>{'The request body should contain the following parameters: amount, walletAddress.'}</Text>
            <View style={{ alignItems: 'flex-end', marginTop: scale(40),marginBottom: scale(40) }}>
                <TouchableOpacity style={{ flexDirection: 'row', marginRight: scale(15) }} onPress={() => props.navigation.navigate('Developers')}>

                    <Text style={{ fontSize: scale(16), fontWeight: '400', color: '#2F5CFC', marginRight: scale(15) }}>Documentation</Text>
                    <AntDesign name="arrowright" size={20} color={'#2F5CFC'} />

                </TouchableOpacity>

            </View>
        </View>
    </View>



}

function mapStateToProps(state) {
    return {

        userInfo: state.auth.userInfo,

    };
}

export default connect(mapStateToProps, {

})(AccountInfo);