/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, Switch, ScrollView,TouchableOpacity, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { useIsFocused } from '@react-navigation/native';
import { HeaderWithBack,Header } from '../../components/Header'
import Logout_PopUp, {Delete_PopUp} from '../../components/Logout_PopUp';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import {CustomProfileHelper} from './helper'
import setAuthToken from '../../config/setAuthToken';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {resetTransaction} from '../../redux/action/home'
import {config, getIosDeviceTokeApi} from '../../config/config';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';

import { createApplicant } from '../../redux/action/auth'
const ProfileScreen = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }))
  }
  useEffect(() => {

  }, []);
const changeTheme1=(val)=>{
  // 
  onChangeTheme({ darkMode: val })
}
const Cancel = () => {
  setIsModalVisible(false)
};

const Logout = async () => {
  setIsModalVisible(false)
  await AsyncStorage.clear();
  setAuthToken('')
  props.resetTransaction()
  props.navigation.navigate('AuthLoading');;
};
const _signout = async () => {
  setIsModalVisible(true)
};
const launchSNSMobileSDK = async() => {

  // From your backend get an access token for the applicant to be verified.
  // The token must be generated with `levelName` and `userId`,
  // where `levelName` is the name of a level configured in your dashboard.
  //
  // The sdk will work in the production or in the sandbox environment
  // depend on which one the `accessToken` has been generated on.
  //
  const data={
    "id": userInfo.email
}
let result = await props.createApplicant(data)
// console.log('result '+JSON.stringify(result))

let accessToken =result?.message?.token;

  let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
     
      // this is a token expiration handler, will be called if the provided token is invalid or got expired
      // call your backend to fetch a new access token (this is just an example)
      cons.log('data ',data)

      return fetch(config().createApplicant, {
        method: 'POST',
        body: JSON.stringify(data),
      }).then(resp => {
console.log('resp '+JSON.stringify(resp))
        // return a fresh token from here
        return resp.message.token
      })
    })
    .withHandlers({ // Optional callbacks you can use to get notified of the corresponding events
      onStatusChanged: (event) => {
        console.log("onStatusChanged: [" + event.prevStatus + "] => [" + event.newStatus + "]");
      },
      onLog: (event) => {
        console.log("onLog: [Idensic] " + event.message);
      }
    })
    .withDebug(true)
    .withLocale('en') // Optional, for cases when you need to override the system locale
    .build();

  snsMobileSDK.launch().then(result => {
    console.log("SumSub SDK State: " + JSON.stringify(result))
  }).catch(err => {
    console.log("SumSub SDK Error: " + JSON.stringify(err))
  });
}
const {userInfo={}}=props
  return (

    <ScrollView   style={{  backgroundColor: 'white' }}>
      <Header
        navigation={props.navigation}
        name={'Profile'}
      />
       

      <View style={{margin:10}}>
      <View style={{  alignItems: 'center' }}>
      <View  style={{ height: 100, width: 100,borderWidth:2,backgroundColor:'#6600ff',borderRadius:64,borderColor:'white',marginBottom:scale(15) }}>
        <Image
        source={localImage.account}
        style={{height: 90, width: 100,borderRadius:64,alignItems:'center'}}
        />
        </View>
         
          <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '700',color:'#6600ff' }}>{userInfo.token?userInfo.firstName + ' '+ userInfo.lastName:'Guest'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', marginTop: scale(10),color:'#6600ff'  }}>{userInfo.token?userInfo.email:''}</Text>
      </View>

      <Logout_PopUp
          Cancel={Cancel}
          Logout={Logout}
          isModalVisible={isModalVisible}
        />
      <View style={{marginTop:scale(20)}}>
      {/* {userInfo.token? <CustomProfileHelper 
            name={'Account Info'}
            textColor={colors.text}
            onNavigate={() =>  props.navigation.navigate('accountInfo')}
            />:null} */}
     
             <CustomProfileHelper 
            name={'Transactions'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Transaction')}
            />
             <CustomProfileHelper 
            name={'Developers'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Developers')}
            />
            
            {userInfo.token?<View>
              {userInfo.isKyc?null:<CustomProfileHelper 
            name={'Kyc'}
            textColor={colors.text}
            onNavigate={() => launchSNSMobileSDK()}

            />}
            
             <CustomProfileHelper 
            name={'Logout'}
            textColor={colors.text}
            onNavigate={() => _signout()}
            />
              </View>:<View>
              <CustomProfileHelper 
            name={'Log in'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Login')}

            />
             <CustomProfileHelper 
            name={'Register'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Register')}

            />
                </View>}
            
            
            
   
      </View>
      {/* <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:scale(20)}}>
      <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>Dark Mode</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#FF4D67" }}
        thumbColor={NavigationTheme.dark ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={(value)=>changeTheme1(value)}
        value={NavigationTheme.dark}
      />
      </View> */}
      

            

      </View>


    </ScrollView>

  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  resetTransaction,createApplicant
})(ProfileScreen);
