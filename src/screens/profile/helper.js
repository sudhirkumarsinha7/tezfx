/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Octicons from 'react-native-vector-icons/Octicons'


import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
// import { Colors } from '../../components/Common/Style'
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Colors } from 'react-native-paper';
/* eslint-disable react-native/no-inline-styles */
export const getIcon =(name,color='#AB92F0')=>{
    if(name==='Account Info'){
      return     <Feather name={'user'} color={'#203C71'} size={25}/>
    }else if(name==='Transactions'){
      return     <FontAwesome5 name={'exchange-alt'} color={'#1DAB87'} size={20}/>
    }else if(name==='Developers'){
      return     <SimpleLineIcons name={'docs'} color={color} size={20}/>
    }else if(name==='Log in'){
      return     <MaterialCommunityIcons name={'login'} color={color} size={25}/>
    }else if(name==='Kyc'){
      return     <Octicons name={'unverified'} color={color} size={25}/>
    }else if(name==='Register'){
        return     <AntDesign name={'adduser'} color={color} size={25}/>
      }else if(name==='Logout'){
        return     <MaterialCommunityIcons name={'logout'} color={color} size={25}/>
      }
      else{
      return     <MaterialCommunityIcons name={'line-scan'} color={color} size={25}/>

    }
  
  }
    
 export  const CustomProfileHelper = (props) => {
    const {NavigationTheme } = useTheme();
    const {colors}=NavigationTheme;
      return (<View style={{padding:scale(10)}}><TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>props.onNavigate()}>
        {getIcon(props.name)}
      <Text  style={{ color: '#203C71', fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>{props.name}</Text>
  
    </TouchableOpacity></View>)
  }
