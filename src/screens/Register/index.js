import React, { useState, useEffect } from "react";
import { ImageBackground, View, Text, StyleSheet, TouchableOpacity, ScrollView, Keyboard, ActivityIndicator } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { config } from "../../config/config";
import { scale } from '../../components/Scale';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';
import { InputTextHelperNew, DropdownView } from '../../components/Common/heper'
import { HeaderWithBackLogo } from '../../components/Header'
import { CustomButton1 } from '../../components/Common/Button'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Register, verifyOtp, resendOtp,createApplicant } from '../../redux/action/auth'
import { connect } from 'react-redux';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';

const CELL_COUNT = 4
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;

const RegisterScreen = (props) => {
  const isFocused = useIsFocused();
  const [isOtpScren, setOtpScren] = useState(false)

  const [loginScreen, setLoginScreen] = useState(false)
  const [err, setErrorMessage] = useState('')
  const [orgName, setOrgName] = useState('')
  const [role, setRole] = useState('')
  const [isMandatory, setMandatory] = useState(false)
  const [isLoader, setLoader] = useState(false)

  // const [isDev, setIsDev] = useState(false)
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [propsc, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  useEffect(() => {

  }, [isFocused]);
  var validationSchema = Yup.object().shape(
    {
      email: Yup.string().matches(reg_email, 'Please Enter valid email id').required(('Required')),
      password: Yup.string()
        .min(6, ('Password must be at least 6 characters'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),
      firstName: Yup.string()
        .min(1, ('FirstName must be at least 1 character'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),
      lastName: Yup.string()
        .min(1, ('LastName must be at least 1 character'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),

    },
    ['email',],
  ); // <-- HERE!!!!!!!!
  const signupform = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      password: "",
      email: "",
      isDeveloper: false,
      role: "",
      organization: ""
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _signUp = () => {
    Keyboard.dismiss();
    setMandatory(true)
    setErrorMessage('');
    signupform.handleSubmit();
  };

  const handleSubmit = async values => {
    console.log('values ', values)
    setLoader(true)
    const result = await props.Register(values);
    setLoader(false)

    console.log('res result' + JSON.stringify(result));
    if (result?.status === 200) {
      //do something
      loginform.handleChange({ target: { name: `email`, value: values.email } })
      loginform.handleChange({ target: { name: `isDeveloper`, value: signupform?.values?.isDeveloper } })

      setOtpScren(true)


    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err1 = result?.data?.message;
      setErrorMessage(err1);

    } else {
      const err2 = result.data;
      const e = err2?.data
      const msg = e.length ? err2.message + ' ' + e[0].msg : err2?.message
      setErrorMessage(msg);

    }
  };
  var validationSchema1 = Yup.object().shape(
    {
      email: Yup.string().matches(reg_email, 'Please Enter valid email id').required(('Required')),

    },
    ['email',],
  ); // <-- HERE!!!!!!!!
  const loginform = useFormik({
    initialValues: {

      otp: "",
      email: "",
      isDeveloper: false,

    },
    validationSchema1,
    onSubmit: (values, actions) => {
      handleSubmit1({ ...values });
    },
  });
  const _login = () => {
    Keyboard.dismiss();
    loginform.handleSubmit();
  };

  const handleSubmit1 = async values => {
    console.log('values ', values)
    setLoader(true)

    const result = await props.verifyOtp(values);
    setLoader(false)

    console.log('res result' + JSON.stringify(result));
    if (result?.status === 200) {
      // props.navigation.navigate('Profile')
      launchSNSMobileSDK()
    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else {
      const err = result.data;
      setErrorMessage(err?.message);

    }
  };
  let launchSNSMobileSDK = async() => {

    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    //
    const data={
      "id": (signupform?.values?.email).toLowerCase()
  }
  let result = await props.createApplicant(data)
  // console.log('result '+JSON.stringify(result))
  
  let accessToken =result?.message?.token;
  if(accessToken){
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
       
      // this is a token expiration handler, will be called if the provided token is invalid or got expired
      // call your backend to fetch a new access token (this is just an example)
      cons.log('data ',data)

      return fetch(config().createApplicant, {
        method: 'POST',
        body: JSON.stringify(data),
      }).then(resp => {
console.log('resp '+JSON.stringify(resp))
        // return a fresh token from here
        return resp.message.token
      })
    })
    .withHandlers({ // Optional callbacks you can use to get notified of the corresponding events
      onStatusChanged: (event) => {
        console.log("onStatusChanged: [" + event.prevStatus + "] => [" + event.newStatus + "]");
      },
      onLog: (event) => {
        console.log("onLog: [Idensic] " + event.message);
      }
    })
    .withDebug(true)
    .withLocale('en') // Optional, for cases when you need to override the system locale
    .build();

  snsMobileSDK.launch().then(result => {
    console.log("SumSub SDK State: " + JSON.stringify(result))
  }).catch(err => {
    console.log("SumSub SDK Error: " + JSON.stringify(err))
    props.navigation.navigate('Profile')
  });
  }else{
    alert('please update kyc')
    props.navigation.navigate('Profile')
  }
  
   
  }
  const resendOtp = async () => {
    setLoader(true)
    let data = {"email":(signupform?.values?.email).toLocaleLowerCase()}
    // let data = {"email":"sudhir1@gmail.in"}

    await props.resendOtp(data)
    setLoader(false)

  }
  const changeCode = (code) => {
    setValue(code);
    loginform.handleChange({ target: { name: `otp`, value: code } })

  }
  const onChangeDev = () => {
    signupform.handleChange({ target: { name: `isDeveloper`, value: !signupform?.values?.isDeveloper } })
    if (signupform?.values?.isDeveloper) {
      signupform.handleChange({ target: { name: `organization`, value: 'N/A' } })
      signupform.handleChange({ target: { name: `role`, value: 'N/A' } })

    } else {
      signupform.handleChange({ target: { name: `organization`, value: '' } })
      signupform.handleChange({ target: { name: `role`, value: '' } })
    }

  }

  const RegisterView = () => {
    return <View >
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Create a account'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'Welcome, Please create your account'}</Text>

      <InputTextHelperNew
        placeholder={'First Name'}
        setValue={signupform.handleChange(`firstName`)}
        value={signupform?.values?.firstName}
        errorMsg={signupform?.errors?.firstName}
        mandatory={isMandatory}
      />

      <InputTextHelperNew
        placeholder={'Last Name'}
        setValue={signupform.handleChange(`lastName`)}
        value={signupform?.values?.lastName}
        errorMsg={signupform?.errors?.lastName}
        mandatory={isMandatory}
      />
      <InputTextHelperNew
        placeholder={'Email'}
        setValue={signupform.handleChange(`email`)}
        value={signupform?.values?.email}
        errorMsg={signupform?.errors?.email}
        mandatory={isMandatory}
      />

      <InputTextHelperNew
        placeholder={'Password'}
        setValue={signupform.handleChange(`password`)}
        value={signupform?.values?.password}
        errorMsg={signupform?.errors?.password}
        mandatory={isMandatory}
        password={true}
      />
      <TouchableOpacity style={{ backgroundColor: '#E9E9E9', padding: scale(15), borderRadius: 8, marginTop: scale(10) }} onPress={() => onChangeDev()}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.9, flexDirection: 'row', alignItems: 'center' }}>
            {signupform?.values?.isDeveloper ? <AntDesign
              name={'checkcircle'}
              color={'green'}
              size={25}
            /> : <Entypo
              name={'circle'}
              color={'gray'}
              size={25} />}

            <Text style={{ textAlign: 'center', marginLeft: scale(10), fontSize: scale(16), fontWeight: '700' }}>{'I am a Developer'}</Text>
          </View>
          <View style={{ justifyContent: 'flex-end', flex: 0.1 }}>
            <MaterialCommunityIcons
              name={'email-outline'}
              color={'gray'}
              size={25}
            />
          </View>

        </View>

      </TouchableOpacity>
      {signupform?.values?.isDeveloper ? <View>
        <InputTextHelperNew
          placeholder={'Organization Name'}
          setValue={signupform.handleChange(`organization`)}
          value={signupform?.values?.organization}
          errorMsg={signupform?.errors?.organization}
          mandatory={isMandatory}
        />
        <InputTextHelperNew
          placeholder={'Role'}
          setValue={signupform.handleChange(`role`)}
          value={signupform?.values?.role}
          errorMsg={signupform?.errors?.role}
        />
      </View> : null}

      <Text style={{ color: 'red', fontSize: scale(14), fontWeight: '400', marginTop: scale(20) }}>{err}</Text>

      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Sign Up')}
          navigation={props.navigation}

          Press={() => _signUp()} />
      </View>

    </View>
  }
  const OTPScreen = () => {
    return <View >
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Verify it’s you'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'We send a code to ( *****@mail.com ). Enter it here to verify your identity'}</Text>



      <CodeField
        ref={ref}
        {...propsc}
        value={value}
        onChangeText={(text) => changeCode(text)}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"

        renderCell={({ index, symbol, isFocused }) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
            onLayout={getCellOnLayoutHandler(index)}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />
            <Text style={{ color: 'red', fontSize: scale(14), fontWeight: '400', marginTop: scale(20) }}>{err}</Text>

      <TouchableOpacity onPress={() => resendOtp()}>

        <Text style={{
          fontSize: scale(16),
          fontWeight: '700',
          color: '#1DAB87',
          margin: scale(15),
          alignSelf: 'center'
        }}>{'Resend Code'} </Text>
      </TouchableOpacity>

      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Confirm')}
          navigation={props.navigation}

          Press={() => _login()} />
      </View>
    </View>
  }
  const DevDetails = () => {
    return <View >
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Developer Details'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'If you are developer, please fill all details'}</Text>

      <InputTextHelperNew
        placeholder={'Organization Name'}
        setValue={(value) => setOrgName(value)}
        value={orgName}
      />
      <InputTextHelperNew
        placeholder={'Role'}
        setValue={(value) => setRole(value)}
        value={role}
      />



      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Sign Up')}
          navigation={props.navigation}

          Press={() => _signUp()} />
      </View>

    </View>
  }

  return (<ScrollView style={{backgroundColor:'white'}}>
    <HeaderWithBackLogo
      navigation={props.navigation}
      name={'Register'}
    />
    <View style={{ margin: scale(15),}}>


      {isOtpScren ? OTPScreen() : RegisterView()}
      {/* {OTPScreen()} */}
      {/* {DevDetails()} */}
      {isLoader ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      ) : null}
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginTop: scale(30), textAlign: 'center' }}>{'Already have an account?'}<TouchableOpacity onPress={() => props.navigation.navigate('Login')}><Text style={{ color: '#1DAB87' }} >{'Sign In'}</Text></TouchableOpacity></Text>

    </View>
  </ScrollView>)

}
const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
  },
  root: { flex: 1, padding: 20 },
  codeFieldRoot: { marginTop: 20, },
  cell: {
    width: 60,
    height: 55,
    color: '#000',
    borderColor: '#D3D3D3',
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    textAlign: 'center',
    fontSize: scale(20),
    paddingTop: scale(10),
  },
  focusCell: {
    borderColor: Colors.blueE9,
  },
  slide: {
    // alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },

});
function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  Register, verifyOtp, resendOtp,createApplicant
})(RegisterScreen);
