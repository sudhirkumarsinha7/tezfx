import React,{useState,useEffect,useRef} from "react";
import { ScrollView,SafeAreaView, View, Text, StyleSheet, TouchableOpacity,Image,TextInput,Pressable,StatusBar } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { DeviceWidth } from "../../config/global";
import { scale } from '../../components/Scale';
import {CustomButton1}  from '../../components/Common/Button'
import AppIntroSlider from 'react-native-app-intro-slider';
const data1 = [
    {
      title: "Simplify Exchanges for Your App with TEZ FX",
      text: "Our mission is to make the entire Web3 ecosystem smarter and more impactful. With TEZ FX, we're empowering microentrepreneurs / SMEs and developers to leverage the benefits of blockchain technology for their businesses.",
      image: localImage.Bitcoins,
      imageStyle: {
  
        width: 313,
        height: 313,
        alignSelf: 'center',
        marginBottom: scale(40),
        marginTop:scale(54)
      },
  
    },
    {
      title: "The easiest way to exchange crypto",
      text: "Blockchain-powered exchange platform that aims to make the entire Web3 ecosystem smarter and more impactful. Our team of experienced developers and blockchain experts are dedicated to empowering businesses and individuals to leverage the power of blockchain technology.",
      image: localImage.blockchain,
      bg: "#febe29",
      imageStyle: {
        width: 293,
        height: 281,
        alignSelf: 'center',
        marginBottom: scale(40),
        marginTop:scale(54)
      },
    },
  ];
export const RenderPagination = (props) => {
  
    const { activeIndex,
      slider,
      data,
      skip } = props
    const handleIntroCompleted = () => {
      console.log('error')
    };
  
    return (
      <View style={styles.paginationContainer}>
        <SafeAreaView>
          <View style={styles.paginationDots}>
            {data.length > 1 &&
              data.map((_, i) => (
                <View>
                  <Pressable
                    key={i}
                    style={[
                      styles.dot,
                      i === activeIndex
                        ? { backgroundColor: "#1D3A70" }
                        : { backgroundColor: 'gray' },
                      i === activeIndex
                        ? { width: 50 }
                        : {},
                    ]}
                    onPress={() => slider?.goToSlide(i, true)}
                  />
                </View>
              ))}
          </View>
  
          <View style={{}}>
  
            {activeIndex === data.length - 1 ? ( 
            <View style={{marginLeft:scale(25),marginRight:scale(25)}}>

            <CustomButton1
            backgroundColor={'#4F46E5'}
            label1={('Get Started')}
            navigation={props.navigation}
           
            Press={skip}
        /></View>
            ) : <TouchableOpacity onPress={skip} style={{ padding: 10,justifyContent:'center',alignItems:'center' }}>
              <Text style={{ color: Colors.gray9, fontSize: 18 }}>{('Skip')}</Text>
            </TouchableOpacity>}
          </View>
        </SafeAreaView>
      </View>
    );
  };
const InitialScreen = (props) => {
    const sliderEl = useRef(null);
    const [landingPage, setlandingPage] = useState(false);

    const keyExtractor = (item) => item.title;
    const onIntroCompleted = () => {
        // navigation.navigate("Root");
      };
    const landingScreen = () => {
        return (<View>
          <AppIntroSlider
            keyExtractor={keyExtractor}
            renderItem={({ item }) => renderItem(item)}
            renderPagination={(activeIndex) => (
              <RenderPagination
                data={data1}
                activeIndex={activeIndex}
                slider={sliderEl.current}
                onIntroCompleted={onIntroCompleted}
                skip={() => props.navigation.navigate('main')}
                // skip={() => props.navigation.navigate('Login')}

              />
            )}
            data={data1}
            ref={sliderEl}
          />
        </View>)
      }
      const renderItem = (item) => (
        <View
          style={[
            styles.slide,
          ]}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
            <Image source={item.image} style={item.imageStyle} />
    
          </View>
          <View style={{ marginBottom: 20, marginLeft: 30 }}>
    
            <Text style={styles.title}>{item.title}</Text>
    
            <Text style={styles.text}>{item.text}</Text>
          </View>
        </View>
      );
    
    return <View  style={styles.imgBackground}>
 <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            justifyContent: 'center',
          }}>
          <StatusBar hidden />

          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              justifyContent: 'center',
            }}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              nestedScrollEnabled={true}
              showsVerticalScrollIndicator={false}>
                {landingScreen()}
                </ScrollView>
                </View>
                </View>
    </View>
}

const styles = StyleSheet.create({
    imgBackground: {
      width: '100%',
      height: '100%',
      // flex: 1,
      backgroundColor:Colors.white
        },
    wrapper: {
      flex: 1,
    },
    root: { flex: 1, padding: 20 },
    codeFieldRoot: { marginTop: 20, },
    cell: {
      width: 60,
      height: 55,
      color: '#000',
      borderColor: '#D3D3D3',
      borderWidth: 1,
      borderBottomWidth: 1,
      borderRadius: 10,
      textAlign: 'center',
      fontSize: scale(20),
      paddingTop: scale(10),
    },
    focusCell: {
      borderColor: Colors.blueE9,
    },
    slide: {
      // alignItems: "center",
      justifyContent: "center",
      padding: 10,
    },
    image: {
      width: 200,
      height: 200,
      alignSelf: 'center',
      marginBottom: scale(40)
    },
    text: {
      color: '#00407B',
      fontSize: scale(11),
      marginBottom: 100,
      marginTop: 30,
      fontWeight: '400',
    },
    title: {
      fontSize: scale(24),
      color: "#1D3A70",
      fontWeight: '700',
    },
    paginationContainer: {
     
    },
    paginationDots: {
      height: 16,
      margin: 16,
      flexDirection: "row",
      justifyContent: 'center',
      alignItems: 'center',
    },
    dot: {
      width: 8,
      height: 8,
      borderRadius: 4,
      marginHorizontal: 4,
      marginTop: 20
  
    },
    buttonContainer: {
      flexDirection: "row",
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 50,
      marginRight: 50,
      marginTop: 20
    },
    button: {
      flex: 1,
      paddingVertical: 20,
      marginHorizontal: 8,
      borderRadius: 24,
      backgroundColor: "#1cb278",
    },
    buttonText: {
      color: "white",
      fontWeight: "600",
      textAlign: "center",
    },
  });
  
export default InitialScreen