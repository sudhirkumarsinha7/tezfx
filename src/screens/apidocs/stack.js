import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();


import ApiDocs from './index';
import Menu from './Drawer';
import Swagger from './Drawer/swager';
import APIKey from './Drawer/apiKey';
import APIEndPoints from './Drawer/apiendpoints';

const Stack = createNativeStackNavigator();
const ApiDocsTab= () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="apidocs"
        component={ApiDocs}
        options={{
          headerShown: false,
        }}
      />
      
    </Stack.Navigator>
  );
};

const ApiDocsScreen = () => {
  return (
    <Drawer.Navigator initialRouteName="apidocs" useLegacyImplementation={true}  screenOptions={{drawerPosition:"right"}} drawerContent={props => <Menu {...props} />}>
      <Drawer.Screen name="apidocs" component={ApiDocsTab} options={{
        headerShown: false
      }} />
      <Stack.Screen
        name="swager"
        component={Swagger}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="apiendpoints"
        component={APIEndPoints}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="apikey"
        component={APIKey}
        options={{
          headerShown: false,
        }}
      />
      
    </Drawer.Navigator>
  );
}
export default ApiDocsScreen;
