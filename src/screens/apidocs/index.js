/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { localImage, DeviceWidth } from '../../config/global';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { HeaderDrawer } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import { Card1 } from '../../components/Card'
import { verticalScale, scale } from '../../components/Scale';
import { PopUp2 } from '../../components/PopUp';


const ApiDocs = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [isdevPopup,setDevPopup] = useState(false)

  useEffect(() => {


  }, [isFocused]);


  const {userInfo={}} = props

  const cancel=()=>{
    setDevPopup(false)
  }

  return (
    <View >
      <HeaderDrawer
        navigation={props.navigation}
        name={'Developer'}
      />
      <ScrollView style={{backgroundColor:'white'}}>
        <View style={{ margin: scale(15) }}>

          <View style={{ borderBottomWidth: 0.5, marginBottom: scale(15) }}>
            <Text style={{ fontSize: scale(20), fontWeight: '600', marginBottom: scale(15) }}>{'TEZ FX API Documentation'}</Text>
          </View>
          <Text style={{ fontSize: scale(14), fontWeight: '400',textAlign: 'justify' }}>{'The TEZ FX API is capable of delivering accurate exchange rate data for more than 385 cryptocurrencies in real-time. Crypto data is gathered from some of the largest cryptocurrency exchanges, requested using HTTP GET and returned in straightforward JSON format. Thanks to a refined fallback algorithm availability, consistency and reliability of crypto data returned by the TEZ FX API are at the highest level.'}</Text>
          <Text style={{ fontSize: scale(14), fontWeight: '400', textAlign: 'justify', marginTop: scale(20) }}>{'The API comes with a series of endpoints, functionalities and options. Find below a technical documentation containing descriptions, integration guides and examples in different programming languages.'}</Text>
          <Text style={{ fontSize: scale(20), fontWeight: '600', marginBottom: scale(15), marginTop: scale(10) }}>{'Quickstart Tool'}</Text>
          <Text style={{ fontSize: scale(14), fontWeight: '400',  textAlign: 'justify' }}>{'No time for a long read? Once signed up for a free or paid subscription plan you will be able to use our Quickstart Tool, allowing you to test any API endpoint at the click of a button.'}</Text>
          <Text style={{ fontSize: scale(14), fontWeight: '400',  textAlign: 'justify', marginTop: scale(10) }}>{'Get a free API access key to start using the Quickstart tool.'}</Text>
          <View style={{marginTop:scale(20)}}>
           <TouchableOpacity style={{ backgroundColor: '#2F5CFC', padding: scale(10), width: '30%', borderRadius: scale(5) }} onPress={() => {userInfo.isDeveloper?props.navigation.navigate('apikey'):setDevPopup(true)}}>
              <Text style={{ fontSize: scale(16), fontWeight: '600', color: 'white', }}>API key</Text>

            </TouchableOpacity>
          </View>
          <View style={{ alignItems: 'flex-end', marginTop: scale(40) }}>
            <TouchableOpacity style={{ flexDirection: 'row', marginRight: scale(15) }} onPress={() => props.navigation.navigate('apiendpoints')}>

              <Text style={{ fontSize: scale(16), fontWeight: '400', color: '#2F5CFC', marginRight: scale(15) }}>Next API Endpoints</Text>
              <AntDesign name="arrowright" size={20} color={'#2F5CFC'} />

            </TouchableOpacity>

          </View>
          <View style={{height:180}} />
        </View>
        <PopUp2
      isModalVisible={isdevPopup}
      image={require('../../assets/error.png')}
      text="Error"
      text1={'Please Register as a Developer to get API Access Key!'}
      label1="Cancel"
      label="Done"
      Ok={() => cancel()}
      cancel={() => cancel()}
    />

      </ScrollView>







    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {
    swap_list: state.home.swap_list,
    buy_list: state.home.buy_list,
    sell_list: state.home.sell_list,
    userInfo: state.auth.userInfo,


  };
}

export default connect(mapStateToProps, {

})(ApiDocs);
