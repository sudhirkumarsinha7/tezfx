/* eslint-disable react-native/no-inline-styles */



import {  HeaderDrawerBack } from '../../../components/Header'
import { View,Text,ScrollView } from 'react-native';
import { URL } from '../../../config/config';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
export const APIView =(props)=>{
    return <View style={{marginTop:scale(15),marginLeft:scale(15),marginRight:scale(15)}}>
 {props.endPoint?<Text style={{fontSize:scale(16),fontWeight:'600',marginBottom:scale(10)}}>{props.endPoint}</Text>:null}
   <Text  style={{fontSize:scale(14),fontWeight:'400', textAlign:'justify'}}>{props.level}</Text>
    <View style={{backgroundColor:'black',padding:scale(10),marginTop:scale(10),marginBottom:scale(10),borderRadius:scale(5)}}>

 
   <Text style={{fontSize:scale(12),fontWeight:'400',color:'white',textAlign:'justify'}}>{props.API}</Text>
   </View>
   <Text  style={{fontSize:scale(14),fontWeight:'400', textAlign:'justify'}}>{props.description}</Text>

    </View>
}
const APIEndPoints=(props) =>{

  return <View  style={{backgroundColor:'white'}}>
  < HeaderDrawerBack
        navigation={props.navigation}
        name={'Developer'}
      />
       <View style={{borderBottomWidth:0.5,marginLeft:scale(15),marginRight:scale(15),marginTop:scale(10)}}>
    <Text style={{fontSize:scale(20),fontWeight:'600',marginBottom:scale(15)}}>{'TEZ FX API Documentation'}</Text>
   </View>
   <ScrollView>
  
    <APIView 
    endPoint={'/submitPayment'}
    level={'This endpoint allows you to submit a payment. Make a POST request to the following URL:'}
    API={URL+'/client/submitPayment'}
    description={'The request body should contain the following parameters: amount, walletAddress    '}
    />
    <APIView 
    endPoint={'/offRamp'}
    level={'This endpoint allows you to initiate an off-ramp transaction. Make a POST request to the following URL:'}
    API={URL+'/client//offRamp'}
    description={'The request body should contain the following parameters: accNo, ifsc, userId, amount, address.    '}
    />
     <APIView 
    endPoint={'/generateApiKey'}
    level={'This endpoint allows you to generate an API key. Make a GET request to the following URL:¯'}
    API={URL+'/client/generateApiKey'}
    />
     <APIView 
    endPoint={'/register'}
    level={'This endpoint allows you to register a new user. Make a POST request to the following URL:    '}
    API={URL+'/client/register'}
    description={'The request body should contain the following parameters: password, firstName, lastName, email.    '}
    />
     <APIView 
    endPoint={'/login'}
    level={'This endpoint allows you to log in as a user. Make a POST request to the following URL:    '}
    API={URL+'/client/login'}
    description={'The request body should contain the following parameter: email.    '}
    />
    <View style={{height:180}}/>
   </ScrollView>
  </View>
 


}

export default APIEndPoints;
