import React, { useEffect, useState } from 'react';
import {
  Button,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Platform,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PopUp2 } from '../../../components/PopUp';
import { localImage } from '../../../config/global';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

const MenuListComponent = (props) => {
  const { img = '', lable } = props

  return <TouchableOpacity
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: verticalScale(15),
    }}
    onPress={() => props.navigateScreen()}>
    {/* <Image
      style={{ width: scale(22), height: scale(22) }}
      source={img}
      resizeMode="contain"
    /> */}
    <Text
      style={{
        fontSize: scale(16),
        marginLeft: scale(15),
        color:'white',
        fontWeight:'600'
      }}>
      {(lable)}
    </Text>
  </TouchableOpacity>
}

const Custom_Side_Menu = (props) => {
 
  const [isdevPopup,setDevPopup] = useState(false)

 
  useEffect(() => {
    async function fetchData() {
      
    }
    fetchData();
  }, []);
  const {userInfo={}} = props
  const cancel=()=>{
    setDevPopup(false)
  }

 
  return (
    <ScrollView  style={{backgroundColor:'#6600ff'}}>
      <SafeAreaView
       
        forceInset={{ top: 'always', horizontal: 'never',}}>
       
      
       <PopUp2
      isModalVisible={isdevPopup}
      image={require('../../../assets/error.png')}
      text="Error"
      text1={'Please Register as a Developer to get API Access Key!'}
      label1="Cancel"
      label="Done"
      Ok={() => cancel()}
      cancel={() => cancel()}
    />

      
        <View style={{ width: '100%',}}>
          <View style={{height:30}}/>

        <Image
              style={{ height: 25, width: 150,marginLeft:scale(15) }}
              source={localImage.logo}
            />
          <MenuListComponent
            // img={require('../../assets/user.png')}
            lable={'Getting Started'}
            navigateScreen={() => {
              props.navigation.navigate('apidocs');
              props.navigation.closeDrawer();
            }}
          />
        <MenuListComponent
            // img={require('../../assets/trophy.png')}
            lable={'API Key'}
            navigateScreen={() => {
              userInfo.isDeveloper?props.navigation.navigate('apikey'):setDevPopup(true)
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            // img={require('../../assets/contactUs.png')}
            lable={'End Points'}
            navigateScreen={() => {
              props.navigation.navigate('apiendpoints');
              props.navigation.closeDrawer();
            }}
          />
          <MenuListComponent
            // img={require('../../assets/contactUs.png')}
            lable={'Swagger Docs'}
            navigateScreen={() => {
              props.navigation.navigate('swager');
              props.navigation.closeDrawer();
            }}
          />
         

         
        </View>
      </SafeAreaView>
    </ScrollView>
  );

}

function mapStateToProps(state) {
  return {
    userInfo: state.auth.userInfo,

  };
}


export default connect(
  mapStateToProps,
  {},
)(Custom_Side_Menu);