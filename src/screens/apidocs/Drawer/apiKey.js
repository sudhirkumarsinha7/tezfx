/* eslint-disable react-native/no-inline-styles */



import {  HeaderDrawerBack } from '../../../components/Header'
import { View, Text, ScrollView, Image } from 'react-native';
import { URL } from '../../../config/config';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { APIView } from './apiendpoints'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { localImage, imageslist, DeviceWidth } from '../../../config/global';

const APIKey = (props) => {
    const { userInfo = {} } = props

    return <View style={{ backgroundColor: 'white' }}>
        < HeaderDrawerBack
            navigation={props.navigation}
            name={'API Key'}
        />


        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderColor: '#C8C8C8', marginTop: scale(20) }}>
            <View style={{ height: 100, width: 100, borderWidth: 2, backgroundColor: '#6600ff', borderRadius: 64, borderColor: 'white', marginBottom: scale(15) }}>
                <Image
                    source={localImage.account}
                    style={{ height: 90, width: 100, borderRadius: 64, alignItems: 'center' }}
                />
            </View>
            <View style={{ justifyContent: 'center', marginLeft: scale(20), alignItems: 'center' }}>
                <Text style={{ fontSize: scale(18), fontWeight: '700', color: '#1D3A70' }}>{userInfo.token ? userInfo.firstName + ' ' + userInfo.lastName : 'Guest'}</Text>
                <Text style={{ fontSize: scale(16), fontWeight: '400', marginTop: scale(10) }}>{userInfo.token ? userInfo.email : ''}</Text>
            </View>
        </View>
        <ScrollView>

        <View style={{ marginTop: scale(15), marginRight: scale(15) }}>
            <View style={{ flexDirection: 'row', marginLeft: scale(15), }}>
                <FontAwesome5 name="key" size={18} color={'#1D3A70'} />

                <Text style={{ fontSize: scale(16), fontWeight: '600', marginBottom: scale(20), color: '#1D3A70', alignSelf: 'center', marginLeft: scale(20) }}>{'Your API Access Key'}</Text>

            </View>

            <APIView
                level={'Your API access key is your unique authentication token used to gain access to the Statwig API.'}
                API={'MCowBQYDK2VwAyEAuyZmPXgZJCuVRW42srAOVUJ6YNmdWjGVWjZb3PAqu90='}
            />
            
            <APIView
                endPoint={'API Access Key'}
                level={"Your API access key is your unique authentication token used to gain access to the tezfx API. You can find yours by logging in to your tezfx account dashboard. \n\nIn order to authenticate with the API, simply append the access_key and secret_keyparameters to the API's base URL and set it to your API access key value."}
                API={'https://sandbox.tezfx.com/backend/api'}
            />
            <APIView
                level={'Append your API access key:Here is an example API call illustrating how to authenticate with the tezfx API:                '}
                API={'curl -X GET "https://sandbox.tezfx.com/backend/api" \ -H "x-api-key: YOUR_ACCESS_KEY" \ -H "x-secret-key: YOUR_SECRET_KEY" \ -H "accept: application/json"'}
            />
            <View style={{height:scale(200)}}/>
        </View>   
             </ScrollView>


    </View>



}

function mapStateToProps(state) {
    return {

        userInfo: state.auth.userInfo,

    };
}

export default connect(mapStateToProps, {

})(APIKey);