/* eslint-disable react-native/no-inline-styles */


import { WebView } from 'react-native-webview';

import {  HeaderDrawerBack } from '../../../components/Header'
import { View,ActivityIndicator } from 'react-native';


const Swagger=(props) =>{

//   return <View>
//   < HeaderDrawerBack
//         navigation={props.navigation}
//         name={'Swagger'}
//       />
//     <WebView source={{ uri: 'https://sandbox.tezfx.com/backend/doc/' }}
//     style={{ marginTop: Platform.OS === 'ios' ? 40 : 0 }} />
//   </View>
 
const ActivityIndicatorLoadingView =()=>{
  <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
}
  return <View style={{flex:1}}>
 < HeaderDrawerBack
            navigation={props.navigation}
            name={'Swagger Docs'}
        />

  <WebView source={{ uri: 'https://sandbox.tezfx.com/backend/doc/' }} 
   javaScriptEnabled={true}
   //For the Cache
   domStorageEnabled={true}
   //View to show while loading the webpage
   renderLoading={ActivityIndicatorLoadingView}
   //Want to show the view or not
   startInLoadingState={true}/>
    </View>
}

export default Swagger;
