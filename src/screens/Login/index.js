import React, { useState, useEffect } from "react";
import { ImageBackground, View, Text, StyleSheet, TouchableOpacity, Image, TextInput, Keyboard } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { DeviceWidth } from "../../config/global";
import { scale } from '../../components/Scale';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';
import { InputTextHelperNew, DropdownView } from '../../components/Common/heper'
import { HeaderWithBackLogo } from '../../components/Header'
import { CustomButton1 } from '../../components/Common/Button'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { LogIn } from '../../redux/action/auth'
import { connect } from 'react-redux';

const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;

const LoginScreen = (props) => {
  const isFocused = useIsFocused();
  const [isMandatory, setMandatory] = useState(false)
  const [error, setErrorMessage] = useState('')
  const [isLoader, setLoader] = useState(false)
  useEffect(() => {

  }, [isFocused]);

  var validationSchema = Yup.object().shape(
    {
      email: Yup.string().matches(reg_email, 'Please Enter valid email id').required(('Required')),
      password: Yup.string()
        .min(6, ('Password must be at least 6 characters'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),

    },
    ['email',],
  ); // <-- HERE!!!!!!!!
  const signinform = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _signin = () => {
    Keyboard.dismiss();
    setMandatory(true)
    signinform.handleSubmit();
  };

  const handleSubmit = async values => {
    console.log('values ', values)
    const result = await props.LogIn(values);

    console.log('res result' + JSON.stringify(result));
    if (result?.status === 200) {
      //do something
      props.navigation.navigate('Profile')

    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else {
      const err = result.data;
      setErrorMessage(err?.message);

    }
  };
  // console.log('userInfo' + JSON.stringify(props.userInfo));

  const LoginView = () => {
    return <View style={{backgroundColor:'white'}}>
      <InputTextHelperNew
        placeholder={'Email'}
        setValue={signinform.handleChange(`email`)}
        value={signinform?.values?.email}
        errorMsg={signinform?.errors?.email}
        mandatory={isMandatory}
      />

      <InputTextHelperNew
        placeholder={'Password'}
        mandatory={isMandatory}
        setValue={signinform.handleChange(`password`)}
        value={signinform?.values?.password}
        password={true}
        errorMsg={signinform?.errors?.password}

      />

      {error ? <Text
        style={{
          color: 'red',
          marginLeft: scale(5),
          marginTop: scale(5),

        }}>
        {error}</Text> : null}

      <TouchableOpacity onPress={() => props.navigation.navigate('forgotPass')}>

        <Text style={{
          fontSize: scale(14),
          fontWeight: '700',
          color: '#1DAB87',
          marginTop: scale(20)
        }}>{'Forgot Password'} </Text>
      </TouchableOpacity>
      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Sign In')}
          navigation={props.navigation}

          Press={() => _signin()} />
      </View>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginTop: scale(30), textAlign: 'center' }}>{'Don’t have an account? '}<TouchableOpacity onPress={() => props.navigation.navigate('Register')}><Text style={{ color: '#1DAB87' }} >{'Sign Up'}</Text></TouchableOpacity></Text>

    </View>
  }

  return (<View  style={{backgroundColor:'white'}}>
    <HeaderWithBackLogo
      navigation={props.navigation}
      name={'Login'}
    />
    <View style={{ margin: scale(15) }}>
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Sign in'} </Text>
      <Text style={{ color: '##6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'Welcome back, Sign in to your account'}</Text>

      {LoginView()}
    </View>
  </View>)

}
const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
  },

});

function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  LogIn
})(LoginScreen);
