import React, { useState, useEffect } from "react";
import { ImageBackground, View, Text, StyleSheet, TouchableOpacity, Image, TextInput, Keyboard } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { DeviceWidth } from "../../config/global";
import { scale } from '../../components/Scale';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { InputTextHelperNew, DropdownView } from '../../components/Common/heper'
import { HeaderWithBackLogo } from '../../components/Header'
import { CustomButton1 } from '../../components/Common/Button'
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { verifyResetOtp, fogetPass, resetPassword } from '../../redux/action/auth'
import { connect } from 'react-redux';
import { isNull } from "util";
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;

const CELL_COUNT = 4

const ForgotPassScreen = (props) => {
  const isFocused = useIsFocused();
  const [email, setEmail] = useState('')
  const [passwd, setPasswd] = useState('')
  const [confPasswd, setConfPasswd] = useState('')
  const [isLoader, setLoader] = useState(false)
  const [error, setErrorMessage] = useState('')
  const [isMandatory, setMandatory] = useState(false)

  const [otpScreen, setOtpScreen] = useState(false)
  const [resetPass, setResetPass] = useState(false)

  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [propsc, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  useEffect(() => {

  }, [isFocused]);

  var validationSchema = Yup.object().shape(
    {
      email: Yup.string().matches(reg_email, 'Please Enter valid email id').required(('Required')),


    },
    ['email',],
  ); // <-- HERE!!!!!!!!
  var validationSchema1 = Yup.object().shape(
    {
      otp: Yup.string()
        .min(4, 'OTP must be 4 characters')
        .max(4, 'OTP must be 4 characters')
        .required('Required'),

    },
    ['otp'],
  ); // <-- HERE!!!!!!!!
  var validationSchema2 = Yup.object().shape(
    {
      password: Yup.string()
        .min(6, 'Password must be at least 6 characters')
        .max(256, 'Too Lonng!')
        .required('Password is Required'),
      confPasswd: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Password and Confirm Password does not match').required('Confirm Password is Required'),


    },
    ['password','confPasswd'],
  ); // <-- HERE!!!!!!!!
  const sendEmailform = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const otpform = useFormik({
    initialValues: {
      email: '',
      otp: ''
    },
    validationSchema: validationSchema1,
    onSubmit: (values, actions) => {
      handleSubmit1({ ...values });
    },
  });
  const resetPassform = useFormik({
    initialValues: {
      email: '',
      password: '',
      confPasswd: ''
    },
    validationSchema: validationSchema2,
    onSubmit: (values, actions) => {
      handleSubmit2({ ...values });
    },
  });
  const _sendOtp = () => {
    Keyboard.dismiss();
    setMandatory(true)
    sendEmailform.handleSubmit();
  };

  const handleSubmit = async values => {
    setLoader(true)
    const result = await props.fogetPass(values);
    setLoader(false)
    if (result?.status === 200) {
      //do something
      otpform.handleChange({ target: { name: `email`, value: values.email } })

      setOtpScreen(true);
      setResetPass(false)
    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else {
      const err = result.data;
      setErrorMessage(err?.message);

    }
  }
  

  const _verifyOtp = () => {
    Keyboard.dismiss();
    setMandatory(true)
    setErrorMessage('');

    otpform.handleSubmit()
  };
  const changeCode = (code) => {
    setValue(code);
    otpform.handleChange({ target: { name: `otp`, value: code } })

  }
  const handleSubmit1 = async values => {
    setLoader(true)
    const result = await props.verifyResetOtp(values);
    console.log('handleSubmit1 result '+JSON.stringify(result) )
    setLoader(false)
    if (result?.status === 200) {
      //do something
      resetPassform.handleChange({ target: { name: `email`, value: values.email } })

      setOtpScreen(false);
      setResetPass(true)
    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else {
      const err = result.data;
      setErrorMessage(err?.message);

    }
  }
 


  const handleSubmit2 = async values => {
    setLoader(true)
    console.log('handleSubmit2 values '+JSON.stringify(values) )

    const result = await props.resetPassword(values);
    console.log('handleSubmit2 result '+JSON.stringify(result) )

    setLoader(false)
    if (result?.status === 200) {
      //do something
      props.navigation.navigate('Profile')

    } else if (result.status === 500) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else if (result?.status === 401) {
      const err = result?.data?.message;
      setErrorMessage(err);

    } else {
      const err = result.data;
      setErrorMessage(err?.message);

    }
  }
  const _restPass = () => {
    Keyboard.dismiss();
    setMandatory(true)
    setErrorMessage('');

    resetPassform.handleSubmit();
  };

  const ForgotPassView = () => {
    return <View >
      <Image source={localImage.lock} style={{ height: 72, width: 78, margin: scale(10) }} />
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Passsword Recovery'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'Enter your registered email below to receive password instructions'}</Text>


      <InputTextHelperNew
        placeholder={'Email'}
        setValue={sendEmailform.handleChange(`email`)}
        value={sendEmailform?.values?.email}
        errorMsg={sendEmailform?.errors?.email}
        mandatory={isMandatory}
      />
      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Send me email')}
          navigation={props.navigation}

          Press={() => _sendOtp()} />
      </View>
    </View>
  }
  const OTPScreen = () => {
    return <View >
      <Image source={localImage.lock} style={{ height: 72, width: 78, margin: scale(10) }} />
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Verify it’s you'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'We send a code to ( *****@mail.com ). Enter it here to verify your identity'}</Text>



      <CodeField
        ref={ref}
        {...propsc}
        value={value}
        onChangeText={(text) => changeCode(text)}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"

        renderCell={({ index, symbol, isFocused }) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
            onLayout={getCellOnLayoutHandler(index)}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />

      {error?<Text style={{fontSize:scale(14),fontWeight:'600',color:'red'}}>{error}</Text>:null}
      <TouchableOpacity onPress={() => _sendOtp()}>

        <Text style={{
          fontSize: scale(16),
          fontWeight: '700',
          color: '#1DAB87',
          margin: scale(15),
          alignSelf: 'center'
        }}>{'Resend Code'} </Text>
      </TouchableOpacity>

      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Confirm')}
          navigation={props.navigation}

          Press={() =>_verifyOtp()} />
      </View>
    </View>
  }
  console.log('resetPassform '+JSON.stringify(resetPassform))
  const resetPassView = () => {
    return <View >
      <Text style={{
        fontSize: 20,
        fontWeight: '700',
        color: '#1D3A70',
        marginBottom: scale(15)
      }}>{'Reset Password'} </Text>
      <Text style={{ color: '#6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(30) }}>{'Please, enter a new password below different from the previous password'}</Text>


      <InputTextHelperNew
        placeholder={'Password'}
        value={resetPassform?.values?.password}
        setValue={resetPassform.handleChange(`password`)}
        errorMsg={resetPassform?.errors?.password}
        password={true}
        mandatory={isMandatory}
      />
      <InputTextHelperNew
        placeholder={'Confirm password'}
        setValue={resetPassform.handleChange(`confPasswd`)}
        value={resetPassform?.values?.confPasswd}
        errorMsg={resetPassform?.errors?.confPasswd}
        password={true}
        mandatory={isMandatory}

      />
            {error?<Text style={{fontSize:scale(14),fontWeight:'600',color:'red'}}>{error}</Text>:isNull}

      <View style={{ marginTop: scale(40) }}>


        <CustomButton1
          backgroundColor={'#4F46E5'}
          label1={('Create new password')}
          navigation={props.navigation}

          Press={() => _restPass()} />
      </View>
    </View>
  }

  return (<View style={{backgroundColor:'white'}}>
    <HeaderWithBackLogo
      navigation={props.navigation}
      name={'ForgotPass'}
    />
    <View style={{ margin: scale(15) }}>


      {/* {resetPassView()} */}
      {/* {OTPScreen()} */}
      {otpScreen?OTPScreen():resetPass? resetPassView():ForgotPassView()}
    </View>
  </View>)

}
const styles = StyleSheet.create({
  imgBackground: {
    width: '100%',
    height: '100%',
  },
  root: { flex: 1, padding: 20 },
  codeFieldRoot: { marginTop: 20, },
  cell: {
    width: 60,
    height: 55,
    color: '#000',
    borderColor: '#D3D3D3',
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    textAlign: 'center',
    fontSize: scale(20),
    paddingTop: scale(10),
  },
  focusCell: {
    borderColor: Colors.blueE9,
  },
  slide: {
    // alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },

});

function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  verifyResetOtp, fogetPass, resetPassword
})(ForgotPassScreen);