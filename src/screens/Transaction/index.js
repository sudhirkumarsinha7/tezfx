/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import {getSwapTransaction,getBuyTransaction,getSellTransaction} from '../../redux/action/home'
import {SelectionSelect1} from '../../components/SelectionSelect'
import { verticalScale,scale } from '../../components/Scale';
import { formatDateDDMMYYYY } from '../../util/util';
import {Empty_Card} from '../../components/Card'
let radioItems = [
  {
    label: 'Buy Crypto',
    selected: true,
  },
  {
    label: 'Sell Crypto',
    selected: false,
  },
  {
    label: 'Swap Crypto',
    selected: false,
  },
]
export const  SwapTransaction = (props) => {
  const {item={}}=props
  return <View style={{ flexDirection: 'row', padding: 10,borderBottomWidth:0.5,borderColor:'#e5e5e5',backgroundColor:'white' }}>
  <View style={{ flex: 0.2,   justifyContent: 'center',}}>
    <View style={{ height: 48, width: 48, borderRadius: 12, backgroundColor:'#FAD4B4', justifyContent: 'center', alignItems: 'center' }}>
    <Text
        style={{
          fontSize: scale(24),
          alignItems: 'center',

        }}>
        {'S'}

      </Text>

    </View>

  </View>
  <View style={{ flex: 0.6, }}>
    <Text
      style={{
        fontSize: scale(12),
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'From Wallet: '+item.fromAddress}</Text>
      <Text
      style={{
        fontSize: scale(12),
        marginTop: scale(5)
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'Transaction Hash: '+item.transactionHash}</Text>
  </View>
  <View style={{ flex: 0.3, justifyContent: 'center' }}>
    <View style={{
      marginRight: 5,
      padding: 5,
      borderRadius: 5,
    }}>
      <Text
        style={{
          fontSize: scale(14),
        alignSelf: 'center',
        marginTop: scale(5),
        fontWeight: '500'

        }}>
        {item.amount+ ' ETH'}

      </Text>
    </View>
     
    <Text
      style={{
       
        fontSize: scale(10),
        alignSelf: 'center',

      }}>
      {formatDateDDMMYYYY(item.timestamp)}
    </Text>
    
  </View>
</View>

}
export const BuyTransaction = (props) => {
  const {item={}}=props
  return <View style={{ flexDirection: 'row', padding: 10,borderBottomWidth:0.5,borderColor:'#e5e5e5',backgroundColor:'white' }}>
  <View style={{ flex: 0.2,   justifyContent: 'center',}}>
    <View style={{ height: 48, width: 48, borderRadius: 12, backgroundColor:'#FAD4B4', justifyContent: 'center', alignItems: 'center' }}>
    <Text
        style={{
          fontSize: scale(24),
          alignItems: 'center',

        }}>
        {'B'}

      </Text>

    </View>

  </View>
  <View style={{ flex: 0.8, }}>
   
    <Text
      style={{
        fontSize: scale(12),
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'Wallet: '+item.walletAddress}</Text>
      <Text
      style={{
        fontSize: scale(12),
        marginTop: scale(5)
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'OrderID: '+item.orderid}</Text>
  </View>
  
</View>

}
export const SellTransaction = (props) => {
  const {item={}}=props
  return <View style={{ flexDirection: 'row', padding: 10,borderBottomWidth:0.5,borderColor:'#e5e5e5',backgroundColor:'white' }}>
  <View style={{ flex: 0.2,   justifyContent: 'center',}}>
    <View style={{ height: 48, width: 48, borderRadius: 12, backgroundColor:'#FAD4B4', justifyContent: 'center', alignItems: 'center' }}>
    <Text
        style={{
          fontSize: scale(24),
          alignItems: 'center',

        }}>
        {'B'}

      </Text>

    </View>

  </View>
  <View style={{ flex: 0.8, }}>
   
    <Text
      style={{
        fontSize: scale(12),
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'Wallet: '+item.walletAddress}</Text>
      <Text
      style={{
        fontSize: scale(12),
        marginTop: scale(5)
      }}
      numberOfLines={3}
      ellipsizeMode="tail">
      {'OrderID: '+item.orderid}</Text>
  </View>
  
</View>

}

const Transactions = (props) => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [selectedTab, setTab] = useState('Buy Crypto');

  useEffect(() => {
    getDetails()

  }, [isFocused]);

  const getDetails =async ()=>{
    if(userInfo.token){
      await props.getSwapTransaction(0,50,userInfo.email)
      await props.getBuyTransaction(0,50,userInfo.email)
     await props.getSellTransaction(0,50,userInfo.email)
    }
    changeActiveRadioButton(0)
  }
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });
    radioItems[index].selected = true;
    const label = radioItems[index].label
    console.log('radioItems[index].label ' + label)
    setTab(label)
  }
 
  const eachTransaction = (item) => {
    return <View>
                {selectedTab === 'Buy Crypto'?<BuyTransaction item={item}/>:selectedTab === 'Sell Crypto'?<SellTransaction item={item}/>:selectedTab ==='Swap Crypto' ?<SwapTransaction item={item}/>:null}
    </View>

  

  }
const emptyView =()=>{
  return <Empty_Card 
  Text={"Sorry, you don't have any transactions here."}/>
}
 
  const {userInfo={}} = props


  return (
    <View  style={{backgroundColor:'white'}}>
      <Header
        navigation={props.navigation}
        name={'Transactions'}
      />

   
      <View style={{ flexDirection: 'row', marginBottom: verticalScale(10), marginTop: verticalScale(20)}}>
        {radioItems.map((item, key) => (
          <SelectionSelect1
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedTextColor={'#4F46E5'}
            
          />
        ))}
        
           </View> 
           <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={selectedTab === 'Buy Crypto'?props.buy_list:selectedTab === 'Sell Crypto'?props.sell_list:selectedTab === 'Swap Crypto'?props.swap_list:[]}
        renderItem={({ item }) => eachTransaction(item)}
        ListEmptyComponent={emptyView}

      />
       

      







    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});



function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,
    swap_list: state.home.swap_list,
    buy_list: state.home.buy_list,
    sell_list: state.home.sell_list,

  };
}

export default connect(mapStateToProps, {
  getSwapTransaction,getBuyTransaction,getSellTransaction})(Transactions);