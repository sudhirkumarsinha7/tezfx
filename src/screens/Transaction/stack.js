import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';


import Transactions from './index';
const Stack = createNativeStackNavigator();
const TransactionsScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Tranasction"
        component={Transactions}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default TransactionsScreen;
