/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, DeviceWidth } from '../../config/global';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import {Card1} from '../../components/Card'
import {getSwapTransaction,getBuyTransaction,getSellTransaction} from '../../redux/action/home'
import {BuyTransaction} from '../Transaction'
import {Empty_Card} from '../../components/Card'

const HomeScreen = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [searchText, setSearchText] = useState('')
  useEffect(() => {
    getDetails()

  }, [isFocused]);
  const getDetails =async ()=>{
    if(userInfo.token){
      await props.getSwapTransaction(0,50,userInfo.email)
      await props.getBuyTransaction(0,50,userInfo.email)
     await props.getSellTransaction(0,50,userInfo.email)
    }
   
  }
  const eachTransaction = (item) => {
  //   return <View style={{ flexDirection: 'row', padding: 10,borderBottomWidth:0.5,borderColor:'#e5e5e5' }}>
  //   <View style={{ flex: 0.2, }}>
  //     <View style={{ height: 48, width: 48, borderRadius: 12, backgroundColor:'#FAD4B4', justifyContent: 'center', alignItems: 'center' }}>
  //     <Text
  //         style={{
  //           fontSize: scale(24),
  //           alignSelf: 'center',

  //         }}>
  //         {'S'}

  //       </Text>

  //     </View>

  //   </View>
  //   <View style={{ flex: 0.5, }}>
  //     <Text
  //       style={{
  //         fontSize: scale(16),
  //         fontWeight: '500',
  //       }}
  //       numberOfLines={2}
  //       ellipsizeMode="tail">
  //       {'Buy Crypto'}
  //     </Text>
  //     <Text
  //       style={{
  //         fontSize: scale(12),
  //         marginTop: scale(5)
  //       }}
  //       numberOfLines={2}
  //       ellipsizeMode="tail">
  //       {'1A1zP1e....Gefi2DMP'}</Text>
  //   </View>
  //   <View style={{ flex: 0.3, }}>
  //     <View style={{
  //       marginRight: 5,
  //       padding: 5,
  //       borderRadius: 5,
  //     }}>
  //       <Text
  //         style={{
  //           fontSize: scale(14),
  //         alignSelf: 'center',
  //         marginTop: scale(5),
  //         fontWeight: '500'

  //         }}>
  //         {'12.35 ETH'}

  //       </Text>
  //     </View>
       
  //     <Text
  //       style={{
         
  //         fontSize: scale(10),
  //         alignSelf: 'center',

  //       }}>
  //       {'17 hr ago'}
  //     </Text>
      
  //   </View>
  // </View>
return <BuyTransaction item={item}/>
  }
 
  const TransactionList = (item) => {
    return <View>
      <View style={{ marginTop: scale(15), flexDirection: 'row',alignItems:'center'  }}>

        <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', flex: 0.5 }}>{'Recent'}</Text>
        <TouchableOpacity
        style={{flexDirection:'row',flex:0.5,alignItems:'center',justifyContent:'flex-end'}}
          onPress={() => props.navigation.navigate('Transaction', { screen: 'Tranasction' })}
        >
                           <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '500',}}>{'All transactions'}</Text>

          <AntDesign name="right" size={20} color={colors.text} />
        </TouchableOpacity>
      </View>
      <FlatList
        // onEndReachedThreshold={0.7}
        horizontal={false}
        keyExtractor={(item, index) => index.toString()}
        data={props.buy_list}
        renderItem={({ item }) => eachTransaction(item)}
        ListEmptyComponent={emptyView}

      />

    </View>
  }

  const HomeTopCommpenent = (item) => {
    return <View>
      <View style={{flexDirection:'row'}}>
        <View style={{flex:0.48}}>
        <Card1
            image={localImage.buy}
            bgcolor="#4F46E5"
            textcolor="#FFFFFF"
            card_name={("Buy Crypto")}
            isDisable={false}
            nextScreen={() =>props.navigation.navigate('BuyCrypto')}

            
          />
        </View>
        <View style={{flex:0.03}}/>
        <View style={{flex:0.48,}}>
        <Card1
            image={localImage.sell}
            bgcolor="#FFFFFF"
            textcolor="#1D3A70"
            border="#1D3A70"
            card_name={("Sell Crypto")}
            nextScreen={() =>props.navigation.navigate('SellCrypto')}
            isDisable={false}
          />
</View>
      </View>
      <View style={{flexDirection:'row',marginTop:scale(5)}}>
        <View style={{flex:0.48}}>
        <Card1
            image={localImage.swap}
            bgcolor="#FFFFFF"
            textcolor="#1D3A70"
            border="#1D3A70"
            card_name={("Swap Crypto")}
            nextScreen={() =>props.navigation.navigate('SwapCrypto')}
            isDisable={false}
          />
        </View>
        <View style={{flex:0.03}}/>
        <View style={{flex:0.48,}}>
        <Card1
            image={localImage.apidocs}
            bgcolor="#1DAB87"
            textcolor="#FFFFFF"
            card_name={("API Docs")}
            nextScreen={() =>props.navigation.navigate('Developers', { screen: 'apidocs' })}
            isDisable={false}
            
          />
</View>
      </View>

    </View>
  }
 
  const HomeScreen=(item)=>{
    return <View>
      {HomeTopCommpenent()}
        {TransactionList()}
       
    </View>
  }
  const emptyView =()=>{
    return <Empty_Card 
    Text={"Sorry, you don't have any transactions here."}/>
  }
  const {userInfo={}} = props
  console.log('userInfo '+JSON.stringify(userInfo))
  return (
    <View>
      <Header
        navigation={props.navigation}
        name={'Welcome back'}
      />
      <View style={{ margin: scale(15) }}>
      <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={['test']}
            renderItem={({ item, index }) => HomeScreen(item, index)}
            ListFooterComponent={<View style={{ height: DeviceWidth }} />}

        />

      

      </View>






    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default HomeScreen;
function mapStateToProps(state) {
  return {
    swap_list: state.home.swap_list,
    buy_list: state.home.buy_list,
    sell_list: state.home.sell_list,
    userInfo: state.auth.userInfo,

    
  };
}

export default connect(mapStateToProps, {
  getSwapTransaction,
  getBuyTransaction,
  getSellTransaction
})(HomeScreen);
