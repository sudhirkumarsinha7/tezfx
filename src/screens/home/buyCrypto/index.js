/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, Keyboard, FlatList,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../../config/global';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { HeaderWithBack } from '../../../components/Header'
import { useTheme } from '../../../theme/Hooks'
import {Card1} from '../../../components/Card'
import { Input, Icon } from '@rneui/themed';
import {InputTextHelperNew } from '../../../components/Common/heper'
import {CustomButton1}  from '../../../components/Common/Button'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {CreateRzpOrder,PayRzpOrder,checkKYc} from '../../../redux/action/home'
import RazorpayCheckout from 'react-native-razorpay';
import { PopUp2, PopUp } from '../../../components/PopUp';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
import axios from "axios";
const SUMSUB_APP_TOKEN='sbx:ifys5XlMycS6BDZKBU37LZYU.nehuy0r8rVevl6vQhZLOrWTDAoHV3lGg'
const SUMSUB_SECRET_KEY='UCmOdiXXDzxkLPszn9GKJusImYGd12wL'
import {config, getIosDeviceTokeApi} from '../../../config/config';
import {getBuyTransaction} from '../../../redux/action/home'

import { createApplicant } from '../../../redux/action/auth'
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
const reg_walletAdress =/^0x[a-fA-F0-9]{40}$/
const BuyCrypto = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [error, setErrorMessage] = useState('')
  const [exchangeRate, setExchangeRate] = useState("");
  const [orderId, setOrderId] = useState("")
  const [loader, setLoader] = useState(false)

  const [isMandatory, setMandatory] = useState(false)
  const [isModalVisible, setIsModalVisible] = useState(false)
  // const [successMessage, setSuccessMessage] = useState('0xC5B0E3C2e9091E8E46B248Ab012323Ae53F082f6')
  const [successMessage, setSuccessMessage] = useState('')

  const {userInfo={}}=props;
  useEffect(() => {
    if(userInfo.email && !buyCryptoForm?.values?.email){
      buyCryptoForm.handleChange({ target: { name: `email`, value: userInfo.email } })

    }
    fetch("https://min-api.cryptocompare.com/data/price?fsym=MATIC&tsyms=INR")
    .then((res) => res.json())
    .then((data) => setExchangeRate(data.INR));
  }, [isFocused]);
  var validationSchema = Yup.object().shape(
    {
      email: Yup.string().matches(reg_email, 'Please Enter valid email id').required(('Required')),
        amount: Yup.number().min(0.1, 'Qauntity must be greater than zero').required('Required'),

        walletAddress: Yup.string().matches(reg_walletAdress, 'Please Enter valid wallet Address').required(('Required')),


    },
    [],
  ); // <-- HERE!!!!!!!!
  const buyCryptoForm = useFormik({
    initialValues: {
      email: "",
      amount: "",
      // walletAddress: "0xC5B0E3C2e9091E8E46B248Ab012323Ae53F082f6",
      walletAddress: "",

    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _buy = () => {
    Keyboard.dismiss();
    setMandatory(true)
    setErrorMessage('');
    buyCryptoForm.handleSubmit();
  };

  const handleSubmit = async values => {
    console.log('values ', values)
    if(userInfo.token){
      setLoader(true)
      const data={
        "id": userInfo.email
    }
     const res= await props.checkKYc(data)
     if(res && res.data &&res.data.message && res.data.message.isKyc)
     {
      const response = await props.CreateRzpOrder(values)
      const orderID= response?.data?.data?.orderid
      setOrderId(orderID)
      _pay(values.amount,orderID)
     }else{
      launchSNSMobileSDK()
     }
     
      setLoader(false)

    }else{
      props.navigation.navigate('Profile',{screen:'Login'})
    }
   
  };
  const launchSNSMobileSDK = async() => {

    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    //
    const data={
      "id": userInfo.email
  }
  let result = await props.createApplicant(data)
  // console.log('result '+JSON.stringify(result))
  
  let accessToken =result?.message?.token;
  
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
       
        // this is a token expiration handler, will be called if the provided token is invalid or got expired
        // call your backend to fetch a new access token (this is just an example)
        cons.log('data ',data)
  
        return fetch(config().createApplicant, {
          method: 'POST',
          body: JSON.stringify(data),
        }).then(resp => {
  console.log('resp '+JSON.stringify(resp))
          // return a fresh token from here
          return resp.message.token
        })
      })
      .withHandlers({ // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: (event) => {
          console.log("onStatusChanged: [" + event.prevStatus + "] => [" + event.newStatus + "]");
        },
        onLog: (event) => {
          console.log("onLog: [Idensic] " + event.message);
        }
      })
      .withDebug(true)
      .withLocale('en') // Optional, for cases when you need to override the system locale
      .build();
  
    snsMobileSDK.launch().then(result => {
      console.log("SumSub SDK State: " + JSON.stringify(result))
    }).catch(err => {
      console.log("SumSub SDK Error: " + JSON.stringify(err))
    });
  }
  const _pay =async(amount,orderID='')=>{
    
    console.log("FROM RESPONSE",orderID)
    var options = {
      description: 'Credits towards consultation',
      image: "https://tezfx.com/static/media/logo-alt.3f946eae21f41b8a48a7.png",
      currency: 'INR',
      key: 'rzp_test_mAOr5LeoRJXmSz', // Your api key
      amount: Math.ceil(exchangeRate * amount * 100).toString(), // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      order_id: orderID,
      name: 'Tezfx',
      prefill: {
        email: userInfo.email||'sudhir@statwig.com',
        contact: '9491136765',
        name: userInfo.firstName||"Test User"
      },
      theme: {color: 'green'}
    }
    RazorpayCheckout.open(options).then( async(response) => {
      // handle success
      // alert(`Success: ${response.razorpay_payment_id}`);
      let newData={
        paymentId: response.razorpay_payment_id,
        orderId: orderId
      }
      setSuccessMessage(response.razorpay_payment_id)
      const paymentResponse = await props.PayRzpOrder(newData)
      await props.getBuyTransaction(0,50,userInfo.email)

      setIsModalVisible(true)
    }).catch((error) => {
      // handle failure
      alert(`Error: ${error.code} | ${error.description}`);
    });
  }
  const cancel = ()=>{
    setIsModalVisible(false)
    buyCryptoForm.resetForm();
    props.navigation.navigate('Home')
  }
  return (
    <View style={{backgroundColor:'white'}}>
      <HeaderWithBack
        navigation={props.navigation}
        name={'Buy Crypto'}
      />
      <View style={{ margin: scale(15) }}>
      <Text style={{color:'##6B7280',fontSize:scale(14),fontWeight:'400',marginBottom:scale(10)}}>{'Hello, Start buying your crypto here'}</Text>
      <InputTextHelperNew 
      img={localImage.inr}
      placeholder={'INR'}
      setValue={(value)=>console.log('')}
      value={'INR'}
      edit={false}
      />
       <InputTextHelperNew 
      icon={'wallet'}
      placeholder={'MATIC'}
      setValue={(value)=>console.log('')}
      value={'MATIC'}
      edit={false}
      />
     
       <InputTextHelperNew
        placeholder={'Amount'}
        setValue={buyCryptoForm.handleChange(`amount`)}
        value={buyCryptoForm?.values?.amount}
        errorMsg={buyCryptoForm?.errors?.amount}
        mandatory={isMandatory}
        KeyboardType={'numeric'}
      />
      
      <InputTextHelperNew
        placeholder={'Wallet ID'}
        setValue={buyCryptoForm.handleChange(`walletAddress`)}
        value={buyCryptoForm?.values?.walletAddress}
        errorMsg={buyCryptoForm?.errors?.walletAddress}
        mandatory={isMandatory}
      />
      <InputTextHelperNew
        placeholder={'Email'}
        setValue={buyCryptoForm.handleChange(`email`)}
        value={buyCryptoForm?.values?.email}
        errorMsg={buyCryptoForm?.errors?.email}
        mandatory={isMandatory}
      />
     <View style={{marginTop:scale(40)}}>

     {loader ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      ) : null}
<CustomButton1
            backgroundColor={'#4F46E5'}
            label1={('Buy')}
            navigation={props.navigation}
           
            Press={()=>_buy()}/>
                  </View>

      </View>
      

      <PopUp
      isModalVisible={isModalVisible}
      image={require('../../../assets/success.png')}
      text="Error"
      text1={successMessage}
      label1="Cancel"
      label="Done"
      Ok={() => cancel()}
      cancel={() => cancel()}
    />



    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {

    userInfo: state.auth.userInfo,

  };
}

export default connect(mapStateToProps, {
  CreateRzpOrder,PayRzpOrder,checkKYc,createApplicant,getBuyTransaction
})(BuyCrypto);
