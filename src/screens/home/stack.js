import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './index';
import Notification from './Notification';
import BuyCrypto from './buyCrypto';
import SellCrypto from './sellCrypto';
import SwapCrypto from './swapCrypto';

const Stack = createNativeStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="BuyCrypto"
        component={BuyCrypto}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="SellCrypto"
        component={SellCrypto}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="SwapCrypto"
        component={SwapCrypto}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{
          headerShown: false,
        }}
      />
       
    </Stack.Navigator>
  );
};
export default HomeStack;
