/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Keyboard, TextInput, FlatList,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../../config/global';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { HeaderWithBack } from '../../../components/Header'
import { useTheme } from '../../../theme/Hooks'
import {Card1} from '../../../components/Card'
import { Input, Icon } from '@rneui/themed';
import {InputTextHelperNew } from '../../../components/Common/heper'
import {CustomButton1}  from '../../../components/Common/Button'
import axios from 'axios';
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { ethers } from "ethers";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {sellTransaction,checkKYc} from '../../../redux/action/home'
const walletReceiverAddr = "0xC5B0E3C2e9091E8E46B248Ab012323Ae53F082f6";
import { PopUp2, PopUp } from '../../../components/PopUp';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
const SUMSUB_APP_TOKEN='sbx:ifys5XlMycS6BDZKBU37LZYU.nehuy0r8rVevl6vQhZLOrWTDAoHV3lGg'
const SUMSUB_SECRET_KEY='UCmOdiXXDzxkLPszn9GKJusImYGd12wL'
import { createApplicant } from '../../../redux/action/auth'
import {config, getIosDeviceTokeApi} from '../../../config/config';
import AsyncStorage from '@react-native-async-storage/async-storage'
import {getSwapTransaction,getBuyTransaction,getSellTransaction} from '../../../redux/action/home'

const SellCrypto = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [loader, setLoader] = useState(false)
  const [error, setError] = useState();
  const [txs, setTxs] = useState([]);
  const [success, setSuccess] = useState();
  const [etherError, setEtherError] = useState("");
  const connector = useWalletConnect(); // valid
  const [connected, setconnected] = useState(false)
  const [walletAddress, setWalletAddress] = useState('0xC5B0E3C2e9091E8E46B248Ab012323Ae53F082f6')

  // const [walletAddress, setWalletAddress] = useState('')
  const [walletAddresslocal, setWalletAddressLocal] = useState('')
  const [errorMsg,setErrorMessage]=useState('')
  const [openEror,setOpenError] = useState(false)
  const [isMandatory, setMandatory] = useState(false)
  const [isModalVisible, setIsModalVisible] = useState(false)

  useEffect(() => {

  }, [isFocused]);
  var validationSchema = Yup.object().shape(
    {
      accName: Yup.string()
        .min(1, ('Account name must be at least 1 character'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),
        accNo: Yup.string()
        .min(4, ('Account number must be at least 4 characters'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),
        ifsc: Yup.string()
        .min(4, ('IFSC number must be at least 4 characters'))
        .max(256, ('Too Lonng!'))
        .required(('Required')),
        ether: Yup.number().min(0.01, 'Qauntity must be greater than zero').required('Required'),

    },
    [],
  ); // <-- HERE!!!!!!!!
  const sellCryptoForm = useFormik({
    initialValues: {
      accName: "",
      accNo: "",
      ifsc: "",
      ether: "",
   
    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _sell = () => {
    Keyboard.dismiss();
    setMandatory(true)
    setErrorMessage('');
    sellCryptoForm.handleSubmit();
  };

  const handleSubmit = async values => {
    console.log('values ', values)
   

    if(userInfo.token){
      setLoader(true)
      const data={
        "id": userInfo.email
    }

     const res= await props.checkKYc(data)
     if(res && res.data &&res.data.message && res.data.message.isKyc)
     {
      startPayment(values)
     }else{
      launchSNSMobileSDK()
     }
     
      setLoader(false)

    }else{
      props.navigation.navigate('Profile',{screen:'Login'})
    }
  };
  const startPayment = async (values) => {
    setLoader(true)
    let amount = values.ether
    console.log('amount'+amount)
    let addr =walletReceiverAddr
    setLoader(true)

    connector.connect()
    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://polygon-rpc.com", 3: "https://polygon-rpc.com" },

      // rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
      timeoutBlocks: 1000,

    });
      
    await provider.enable();
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    // console.log('connector'+JSON.stringify(connector))
    setWalletAddressLocal(connector.accounts[0])
    console.log('connector.accounts[0]' + JSON.stringify(connector.accounts[0]))

      const signer = ethers_provider.getSigner();
      ethers.utils.getAddress(addr);
      const tx = await signer.sendTransaction({
        to: addr,
        value: ethers.utils.parseEther(amount),
      });
      console.log({ amount, addr });
      console.log("tx", tx);
      setTxs([tx]);
      const rates = await axios.get(
        "https://api.sendwyre.com/v3/rates?pretty&as=priced"
      );
      const rateInInr = rates.data.USDETH.USD * 75 * amount * 0.95;
      console.log("rateInInr", tx);

      setIsModalVisible(true)
      values.address=tx
      values.amount=rateInInr
      const res1= await props.sellTransaction(values)
      setLoader(false)

      setSuccess(
        `       Crypto payment initiated. ${rateInInr.toFixed(
          2
        )} will be credited soon.`
      );
   
  };
  const launchSNSMobileSDK = async() => {

    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    //
    const data={
      "id": userInfo.email
  }
  let result = await props.createApplicant(data)
  // console.log('result '+JSON.stringify(result))
  
  let accessToken =result?.message?.token;
  
    let snsMobileSDK = SNSMobileSDK.init(accessToken, async() => {
       
        // this is a token expiration handler, will be called if the provided token is invalid or got expired
        // call your backend to fetch a new access token (this is just an example)
        cons.log('data ',data)
  
        return fetch(config().createApplicant, {
          method: 'POST',
          body: JSON.stringify(data),
        }).then(resp => {
  console.log('resp '+JSON.stringify(resp))
          // return a fresh token from here
          return resp.message.token
        })
      })
      .withHandlers({ // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: (event) => {
          console.log("onStatusChanged: [" + event.prevStatus + "] => [" + event.newStatus + "]");
        },
        onLog: (event) => {
          console.log("onLog: [Idensic] " + event.message);
        }
      })
      .withDebug(true)
      .withLocale('en') // Optional, for cases when you need to override the system locale
      .build();
  
    snsMobileSDK.launch().then(result => {
      console.log("SumSub SDK State: " + JSON.stringify(result))
    }).catch(err => {
      console.log("SumSub SDK Error: " + JSON.stringify(err))
    });
  }
  const cancel = ()=>{
    setIsModalVisible(false)
    sellCryptoForm.resetForm();
    props.navigation.navigate('Home')
  }
  const {userInfo={}}=props;

  return (
    <View style={{backgroundColor:'white'}}>
      <HeaderWithBack
        navigation={props.navigation}
        name={'Sell Crypto'}
      />
      <View style={{ margin: scale(15) }}>
      <Text style={{color:'##6B7280',fontSize:scale(14),fontWeight:'400',marginBottom:scale(10)}}>{'Hello, Start selling your crypto here'}</Text>
      
     
      <InputTextHelperNew
        placeholder={'Bank Account Name'}
        setValue={sellCryptoForm.handleChange(`accName`)}
        value={sellCryptoForm?.values?.accName}
        errorMsg={sellCryptoForm?.errors?.accName}
        mandatory={isMandatory}
      />
      <InputTextHelperNew
        placeholder={'Bank Account No'}
        setValue={sellCryptoForm.handleChange(`accNo`)}
        value={sellCryptoForm?.values?.accNo}
        errorMsg={sellCryptoForm?.errors?.accNo}
        mandatory={isMandatory}
      />
     
     
      <InputTextHelperNew
        placeholder={'IFSC Code'}
        setValue={sellCryptoForm.handleChange(`ifsc`)}
        value={sellCryptoForm?.values?.ifsc}
        errorMsg={sellCryptoForm?.errors?.ifsc}
        mandatory={isMandatory}
      />
      <InputTextHelperNew
        placeholder={'Amount in Matic'}
        setValue={sellCryptoForm.handleChange(`ether`)}
        value={sellCryptoForm?.values?.ether}
        errorMsg={sellCryptoForm?.errors?.ether}
        mandatory={isMandatory}
        KeyboardType={'numeric'}
      />
        <PopUp2
      isModalVisible={isModalVisible}
      image={require('../../../assets/success.png')}
      text="Error"
      text1={success}
      label1="Cancel"
      label="Done"
      Ok={() => cancel()}
      cancel={() => cancel()}
    />



     <View style={{marginTop:scale(40)}}>
     {loader ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator color="#0000ff" />
        </View>
      ) : null}

<CustomButton1
            backgroundColor={'#4F46E5'}
            label1={('Sell')}
            navigation={props.navigation}
           
            Press={()=>_sell()}/>
                  </View>

      </View>
      





    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {
    userInfo: state.auth.userInfo,


  };
}

// export default connect(mapStateToProps, {
//   sellTransaction,checkKYc,createApplicant
// })(SellCrypto);


export default withWalletConnect(connect(mapStateToProps, {
  sellTransaction,checkKYc,createApplicant,getSellTransaction
})(SellCrypto), {
  clientMeta: {
    description: "Connect with WalletConnect",
  },
  redirectUrl:
    Platform.OS === "web" ? window.location.origin : "yourappscheme://",
  storageOptions: {
    asyncStorage: AsyncStorage,
  },
});
