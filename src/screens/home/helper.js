/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import { Colors } from '../../components/Common/Style'
/* eslint-disable react-native/no-inline-styles */
function getPreviousDay(date = new Date()) {
  const previous = new Date(date.getTime());
  previous.setDate(date.getDate() - 1);

  return previous;
}
import { useCountdown } from '../../hooks/useCountdown';
const ShowCounter = ({ days, hours, minutes, seconds,islarge }) => {
  const timerDesig = (val) => {
    return <View style={{ flexDirection: 'row', backgroundColor: Colors.red00, padding:islarge?scale(10): scale(2), paddingLeft:islarge?scale(20): scale(6), paddingRight:islarge?scale(20): scale(6), borderRadius: islarge?scale(8):scale(2) }}>
      <Text style={{ color: Colors.whiteFF }}>{val + ''}</Text>
    </View>
  }
  return (
    <View style={{ flexDirection: 'row',  }}>
      {timerDesig(days)}
      <Text style={{ color: Colors.red00, fontSize: islarge?scale(26):scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(hours)}
      <Text style={{ color: Colors.red00, fontSize:islarge?scale(26): scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(minutes)}
      <Text style={{ color: Colors.red00, fontSize: islarge?scale(26):scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(seconds)}
    </View>
  );
};
export const CountdownTimer = ({ targetDate,islarge=false }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  if (days + hours + minutes + seconds <= 0) {
    return <ShowCounter
      days={'00'}
      hours={'00'}
      minutes={'00'}
      seconds={'00'}
      islarge={islarge}
    />;
  } else {
    return (
      <ShowCounter
        days={days}
        hours={hours}
        minutes={minutes}
        seconds={seconds}
        islarge={islarge}

      />
    );
  }
};







