/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { GallaryList, ModalList, DeviceWidth } from '../../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../../components/Common/Button';
import { Header } from '../../../components/Header'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../../theme/Hooks'

const Notifications = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  useEffect(() => {

  }, []);
  
  const eachNotification =(item)=>{
    return <TouchableOpacity style={{flexDirection:'row', padding:10,borderRadius:10,marginRight:scale(10),}} onPress={() => props.navigation.navigate('Home', { screen: 'ViewSoldNF',params: { item} })}>
        <View style={{flex:0.2}}>
        <Image source={item.image} 
        resizeMode={'stretch'}
        style={{ width: scale(50), height: scale(50),borderRadius:10 }} 
        />

        </View>
        <View style={{flex:0.8,}}>
        <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700',}}>{'Harish Upload in Market'}</Text>
        <View style={{}}>

          
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400',marginTop:scale(5)}}>{'Item #526723 Water'}  </Text>
        <Text style={{ color: colors.text, fontSize: scale(10), fontWeight: '400',marginTop:scale(5)}}>{'1 day ago'}  </Text>

      </View>
        </View>
    </TouchableOpacity>
  }

  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: 10 }}>

      <FlatList
            // onEndReachedThreshold={0.7}
            horizontal={false}
            keyExtractor={(item, index) => index.toString()}
            data={GallaryList}
            renderItem={({ item }) => eachNotification(item)}
            ListFooterComponent={<View style={{ height: DeviceWidth }} />}

          />
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(Notifications);
