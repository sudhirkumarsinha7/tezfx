/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../../config/global';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useIsFocused } from '@react-navigation/native';
import { HeaderWithBack } from '../../../components/Header'
import { useTheme } from '../../../theme/Hooks'
import { Card1 } from '../../../components/Card'
import { Input, Icon } from '@rneui/themed';
import { InputTextHelperNew, DropdownView } from '../../../components/Common/heper'
import { CustomButton1 } from '../../../components/Common/Button'
import { getCurrencyList, getConversionRate, validateAddresss, createSwap, createSwapTransaction } from '../../../redux/action/home'
import axios from 'axios';
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { ethers } from "ethers";
import abi from "./abi";
import { PopUp2, PopUp } from '../../../components/PopUp';
import AsyncStorage from '@react-native-async-storage/async-storage'
import {getSwapTransaction} from '../../../redux/action/home'

const SwapCrypto = props => {
  const isFocused = useIsFocused();
  const connector = useWalletConnect(); // valid
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [sourceCrypto, setsourceCrypto] = useState('')
  const [destinationCrypto, setdestinationCrypto] = useState('')
  const [srcCrypto, setsrcCrypto] = useState('')
  const [desCrypto, setdesCrypto] = useState('')
  const [amountError, setAmountError] = React.useState(null);
  const [exchangeRate, setExchangeRate] = React.useState(null);
  const [destinationError, setDestinationError] = React.useState(null);
  const [walletAddressError, setWalletAddressError] = React.useState(null);
  const [amount, setAmount] = useState('')
  const [transaction, setTransaction] = React.useState({});
  const [connected, setconnected] = useState(false)
  // const [walletAddress, setWalletAddress] = useState('0xC5B0E3C2e9091E8E46B248Ab012323Ae53F082f6')
  const [walletAddress, setWalletAddress] = useState('')

  const [walletAddresslocal, setWalletAddressLocal] = useState('')
  const [errorMsg, setErrorMessage] = useState('')
  const [openEror, setOpenError] = useState(false)
  const [loader, setLoader] = useState(false)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [success, setSuccessMessage] = useState(false)

  useEffect(() => {
    props.getCurrencyList()

  }, [isFocused]);
  const onDisConnect = () => {
    connector.killSession()
    setconnected(false)
  }
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (amount && sourceCrypto && destinationCrypto) {
          let data = {
            sourceCrypto: sourceCrypto,
            destCrypto: destinationCrypto,
            amount: amount,
          }
          const res = await props.getConversionRate(data)

          console.log('getConversionRate res' + JSON.stringify(res));

          if (res.message.amountTo < 0.01 || res.message.error) {
            setAmountError(res.message.message);
            setExchangeRate(null)
          } else {
            setExchangeRate(res.message);
            setAmountError(null);
          }
        }
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, [amount, sourceCrypto, destinationCrypto]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        setDestinationError(null);
        if (walletAddress && destinationCrypto) {
          let data = {
            walletAddress: walletAddress,
            destination: desCrypto.ticker
          }
          const res = await props.validateAddresss(data)
          console.log(res);
          if (!res.message.result) {
          } else {
            setWalletAddressError(false);
          }
        }
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, [walletAddress, destinationCrypto]);
  const onChangeSCrypto = item => {
    setsourceCrypto(item.value)
    setsrcCrypto(item.eachItem)
  };
  const onChangeDCrypto = item => {
    setdestinationCrypto(item.value)
    setdesCrypto(item.eachItem)

  };
  const _swapCrypto = async () => {
    if (userInfo.token) {
      setLoader(true)
      //   const data={
      //     "id": userInfo.email
      // }
      //  const res= await props.checkKYc(data)
      //  if(res && res.data &&res.data.message && res.data.message.isKyc)
      //  {
      //   let data ={
      //     destination: desCrypto.ticker,
      //     source: srcCrypto.ticker,
      //     destinationAddress: walletAddress,
      //     amount: amount,
      //     refundAddress: walletAddress,
      //     quotaId: exchangeRate.quotaId,
      //   }
      //   setLoader(true)
      //   const res = await props.createSwap(data)
      //   console.log('_swapCrypto '+JSON.stringify(res))
      //   if (res.message.error) {
      //     setDestinationError(res.message.message);
      //   } else {
      //     initiatePayment(res.message.transaction);
      //   }
      //  }else{

      //  }
      let data = {
        destination: desCrypto.ticker,
        source: srcCrypto.ticker,
        destinationAddress: walletAddress,
        amount: amount,
        refundAddress: walletAddress,
        quotaId: exchangeRate.quotaId,
      }
      setLoader(true)
      const res = await props.createSwap(data)
      console.log('_swapCrypto ' + JSON.stringify(res))
      if (res.message.error) {
        setDestinationError(res.message.message);
      } else {
        initiatePayment(res.message.transaction);
      }
      setLoader(false)

    } else {
      props.navigation.navigate('Profile', { screen: 'Login' })
    }



  }
  async function initiatePayment(transaction) {
    setTransaction(transaction);
    console.log(' srcCrypto.smartContract ' + srcCrypto.smartContract)
    if (
      srcCrypto.smartContract != null &&
      srcCrypto.smartContract.length > 5
    )
      handleTokenCrypto(transaction);
    else handleCryptoNative(transaction);
  }
  async function handleTokenCrypto(txn) {
    try {
      const addr = txn.addressDeposit;

      connector.connect()
      const provider = new WalletConnectProvider({
        bridge: "https://bridge.walletconnect.org",
        rpc: { 1: "https://polygon-rpc.com", 3: "https://polygon-rpc.com" },

        // rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
        infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
        connector: connector,
        connected: true,
        qrcode: false,
        timeoutBlocks: 1000,

      });
      await provider.enable();
      setWalletAddressLocal(connector.accounts[0])
      setLoader(true)

      setconnected(true)
      const ethers_provider = new ethers.providers.Web3Provider(provider);

      const tokenContract = srcCrypto.smartContract;
      console.log("Source Crypto", srcCrypto);
      console.log("SC ADDR", tokenContract);
      const toAddress = txn.addressDeposit;
      const ether = txn.amountDeposit;
      const signer = ethers_provider.getSigner();
      const contract = new ethers.Contract(tokenContract, abi, ethers_provider);
      const userAddress = await signer.getAddress();

      const userTokenBalance = await contract.balanceOf(userAddress);
      const amount = ethers.utils.parseUnits(ether, 18);
      setLoader(false)
      console.log(('userTokenBalance' + JSON.stringify(userTokenBalance)))

      console.log(('userTokenBalance.lt(amount)' + JSON.stringify(userTokenBalance.lt(amount))))

      if (userTokenBalance.lt(amount)) {
        alert("Insufficient balance.");
        setErrorMessage("Insufficient balance.");
        setOpenError(true);
        return;
      }
      ethers.utils.getAddress(toAddress);
      const data = contract.interface.encodeFunctionData("transfer", [
        toAddress,
        amount,
      ]);
      const contractSigner = contract.connect(signer);

      //Define tx and transfer token amount to the destination address
      const tx = await contractSigner.transfer(toAddress, amount);

      console.log({ ether, toAddress });
      console.log("tx", tx);
      let data1 = {
        transactionHash: tx?.hash,
        fromAddress: userAddress,
        toAddress: addr,
        amount: ether
      }
      setLoader(true)

      await props.createSwapTransaction(data1);
      setLoader(false)

      setIsModalVisible(true)
      setSuccessMessage('Amoumt swapped successfully')

    } catch (err) {
      console.error(err);
      setErrorMessage("Something went wrong. please try again later.");
      setOpenError(true);
    }
  }

  async function handleCryptoNative(txn) {
    try {

      const addr = txn.addressDeposit;
      connector.connect()
      const provider = new WalletConnectProvider({
        bridge: "https://bridge.walletconnect.org",
        rpc: { 1: "https://polygon-rpc.com", 3: "https://polygon-rpc.com" },

        // rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
        infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
        connector: connector,
        connected: true,
        qrcode: false,
        timeoutBlocks: 1000,

      });
      await provider.enable();
      const ethers_provider = new ethers.providers.Web3Provider(provider);
      // console.log('connector'+JSON.stringify(connector))
      setWalletAddressLocal(connector.accounts[0])
      console.log('connector.accounts[0]' + JSON.stringify(connector.accounts[0]))

      setconnected(true)
      const ether = txn.amountDeposit;
      //"0.01"
      const signer = ethers_provider.getSigner();
      const userAddress = await signer.getAddress();
      const userEthBalance = await ethers_provider.getBalance(userAddress);
      const parsedEther = ethers.utils.parseEther(ether);
      console.log('userAddress' + JSON.stringify(userAddress))

      console.log('parsedEther' + JSON.stringify(parsedEther))
      console.log('userEthBalance' + JSON.stringify(userEthBalance))
      console.log('(userEthBalance.lt(parsedEther)' + JSON.stringify((userEthBalance.lt(parsedEther))))

      if (userEthBalance.lt(parsedEther)) {
        alert("Insufficient balance.");

        setErrorMessage("Insufficient balance.");
        setOpenError(true);
        return;
      }
      ethers.utils.getAddress(addr);
      const tx = await signer.sendTransaction({
        to: addr,
        value: ethers.utils.parseEther(ether),
      });
      console.log({ ether, addr });
      console.log("tx", tx);
      let data = {
        transactionHash: tx?.hash,
        fromAddress: userAddress,
        toAddress: addr,
        amount: ether
      }
      await props.createSwapTransaction(data);
      setSuccessMessage('Amoumt swapped successfully')

      setIsModalVisible(true)
    } catch (err) {
      console.error(err);
      setErrorMessage("Something went wrong, please try again.");
      setOpenError(JSON.stringify(true));
    }
  }

  const cancel = async() => {
    setIsModalVisible(false)
    await props.getSwapTransaction(0,50,userInfo.email)

    props.navigation.navigate('Home')
  }

  const isFormValid = walletAddress && amount && amount > 0;
  const { userInfo = {} } = props;

  return (
    <View style={{backgroundColor:'white'}}>
      <HeaderWithBack
        navigation={props.navigation}
        name={'Swap Crypto'}
      />
      <ScrollView style={{ margin: scale(15) }}>
        <Text style={{ color: '##6B7280', fontSize: scale(14), fontWeight: '400', marginBottom: scale(10) }}>{'Hello, Start Swaping your crypto here'}</Text>
        <DropdownView
          dropDowndata={props.currency_list}
          onChangeValue={onChangeSCrypto}
          placeholder={'SOURCE CRYPTO'}
          value={sourceCrypto}
          label='name'
          mapKey='ticker'

        />
        <InputTextHelperNew
          placeholder={'Amount'}
          setValue={(value) => setAmount(value)}
          value={amount}
          errorMsg={amountError}
          mandatory={true}
          KeyboardType={'numeric'}
        />
        <View style={{ alignItems: 'center', justifyContent: 'center', margin: scale(30) }}>

          <MaterialCommunityIcons name={'swap-vertical-circle-outline'} size={42} />

        </View>
        <DropdownView
          dropDowndata={props.currency_list}
          onChangeValue={onChangeDCrypto}
          placeholder={'DESTINATION CRYPTO'}
          value={destinationCrypto}
          label='name'
          mapKey='ticker'
        />
        <InputTextHelperNew
          placeholder={'Wallet ID'}
          setValue={(value) => setWalletAddress(value)}
          value={walletAddress}
        />

        {exchangeRate && exchangeRate.amountTo ? <View style={{ backgroundColor: '#1DAB87', borderRadius: 20, marginTop: scale(20) }}>
          <Text style={{ color: '#FFFFFF', fontSize: scale(14), fontWeight: '500', padding: scale(10), paddingTop: scale(30) }}>{'You will get'}</Text>
          <Text style={{ color: '#FFFFFF', fontSize: scale(32), fontWeight: '700', padding: scale(10), paddingBottom: scale(30) }}>{exchangeRate?.amountTo} {destinationCrypto}</Text>

        </View> : null}

        <PopUp2
          isModalVisible={isModalVisible}
          image={require('../../../assets/success.png')}
          text="Error"
          text1={success}
          label1="Cancel"
          label="Done"
          Ok={() => cancel()}
          cancel={() => cancel()}
        />
        <View style={{ marginTop: scale(40) }}>

          {loader ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator color="#0000ff" />
            </View>
          ) : null}
          <CustomButton1
            backgroundColor={'#4F46E5'}
            label1={('Swap')}
            enable={isFormValid}
            navigation={props.navigation}

            Press={() => _swapCrypto()} />
        </View>
        <View style={{ height: scale(140) }} />


      </ScrollView>






    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {
    currency_list: state.home.currency_list,
    userInfo: state.auth.userInfo,


  };
}

// export default connect(mapStateToProps, {
//   getCurrencyList,
//   getConversionRate,
//   validateAddresss,
//   createSwap,
//   createSwapTransaction
// })(SwapCrypto);


export default withWalletConnect(connect(mapStateToProps, {
  getCurrencyList,
  getConversionRate,
  validateAddresss,
  createSwap,
  createSwapTransaction,getSwapTransaction
})(SwapCrypto), {
  clientMeta: {
    description: "Connect with WalletConnect",
  },
  redirectUrl:
    Platform.OS === "web" ? window.location.origin : "yourappscheme://",
  storageOptions: {
    asyncStorage: AsyncStorage,
  },
});
