/* eslint-disable no-sparse-arrays */
import { Dimensions } from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;

export const localImage = {
  loading:{ image: require('../assets/loading.gif') },
  Alert:require('../assets/Imgs/Alert.png'),
  alertstatus:require('../assets/Imgs/alertstatus.png'),
  apidocs:require('../assets/Imgs/apidocs.png'),
  back:require('../assets/Imgs/back.png'),
  Bitcoins:require('../assets/Imgs/Bitcoins.png'),
  blockchain:require('../assets/Imgs/blockchain.png'),
  buy:require('../assets/Imgs/buy.png'),
  check:require('../assets/Imgs/check.png'),
  eyeoff:require('../assets/Imgs/eyeoff.png'),
  eyeon:require('../assets/Imgs/eyeon.png'),
  lock:require('../assets/Imgs/lock.png'),
  logo:require('../assets/Imgs/logo.png'),
  sell:require('../assets/Imgs/sell.png'),
  swap:require('../assets/Imgs/swap.png'),
  swapping:require('../assets/Imgs/swapping.png'),
  TEZFX:require('../assets/Imgs/TEZFX.png'),
  inr:require('../assets/Imgs/inr.png'),
  logo1:require('../assets/Imgs/logo1.png'),
  backgraound:require('../assets/backgraound.png'),
  account:require('../assets/account.png'),
  
};
export const userInfo = {
  name: 'Xyz ',
  email: 'abc@gmail.com',
  comment: 'Why Netflix & Chill when you can NFT & Chill? 😎',
};
export const userNftList = [
  { name: 'User Created', id: 1 },
  { name: 'User Purchase', id: 2 },
  { name: 'User Unlisted', id: 3 },
  { name: 'My Collection', id: 4 },
  { name: 'Favarotes', id: 5 },
];
export const Colors = {
  red00: 'red ',
  blue: 'blue',
  black: 'black',
};
