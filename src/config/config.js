

/* eslint-disable sort-keys */
// export const DEV_URL = 'https://nft.statledger.io';
import { Buffer } from 'buffer';




export const URL = 'https://sandbox.tezfx.com/backend/api';

export const getIosDeviceTokeApi =
  'https://iid.googleapis.com/iid/v1:batchImport';
export const projectid = 'com.thegmrcargo';

/*Infura setup start*/
export const projectId = '2DTf2K1f9CHbXDp04vtmg06MLKz';   // <---------- your Infura Project ID
export const projectSecret = '711d290b49ffe6e0f1196f980a52489a';  // <---------- your Infura Secret
// (for security concerns, consider saving these values in .env files)
export const auth = 'Basic ' + Buffer.from(projectId + ':' + projectSecret).toString('base64');
/*Infura setup end*/
//https://tollytokens.com/api/catalogue_service/items/items?state=PURCHASED&userId=USR-10036&user_id=62bee1c13837e30d6
//https://tollytokens.com/api/catalogue_service/items/items?state=ON_SALE&userId=USR-10036&user_id=62bee1c13837e30d62111465
//https://tollytokens.com/api/catalogue_service/items/items?state=UNLISTED&userId=USR-10036&user_id=62bee1c13837e30d62111465
//https://tollytokens.com/api/catalogue_service/items/items?state=ON_SALE&userId=USR-10036&user_id=62bee1c13837e30d62111465
export const global = {
  supportMail: 'dev@statwig.com',
  supportPh: '+919059687874',
  GoogleMapsApiKey: 'AIzaSyBLwFrIrQx_0UUAIaUwt6wfItNMIIvXJ78',
  region: {
    latitude: 17.4295865,
    longitude: 78.368776,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }
};
export function config() {
  const confs = {
    dev: {
     
      register: `${URL}/auth/register`,
      login: `${URL}/auth/login`,
      devlogin: `${URL}/auth/devlogin`,
      verifyOtp: `${URL}/auth/verify-otp`,
      resendOtp: `${URL}/auth/resend-verify-otp`,
      verifyResetOtp: `${URL}/auth/verify-reset-otp`,
      forgotPassword: `${URL}/auth/forgot-password`,
      resetPassword: `${URL}/auth/reset-password`,
      createApplicant: `${URL}/kyc/createApplicant`,
      getApplicant: `${URL}/kyc/getApplicant`,
      getTransactions: `${URL}/swap/getTransactions?`,
      buyTrans: `${URL}/order/buy?`,
      sellTrans: `${URL}/order/sell?`,
      rzpOrder: `${URL}/order/rzpOrder`,
      getCurrencyList: `${URL}/swap/getCurrencyList`,
      getConversionRate: `${URL}/swap/getConversionRate`,
      validateAddresss: `${URL}/swap/validateAddress`,
      createSwap: `${URL}/swap/createSwap`,
      swapTransaction: `${URL}/swap/swapTransaction`,
      offRamp: `${URL}/order/offRamp`,
      pay: `${URL}/wallet/pay/`,
    }
      
  };

  const conf = confs.dev;

  return conf;
}
