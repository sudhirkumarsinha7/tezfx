/* eslint-disable sort-keys */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {USERINFO,LOGIN,REGISTER} from '../constant';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {config, getIosDeviceTokeApi,auth} from '../../config/config';

import setAuthToken from '../../config/setAuthToken';
import {loadingOff, loadingOn} from './loader';

export const Register = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sRegister_data' + JSON.stringify(data));
    let response = await axios.post(config().register, data);

    console.log('Action_sRegister_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_Register_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const LogIn = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_sLogIn_data' + JSON.stringify(data));
    let response = await axios.post(config().login, data);

    console.log('Action_LogIn_response' + JSON.stringify(response.data.data));
    if(response?.data?.data?.token){
      await AsyncStorage.setItem('token', response?.data?.data?.token);

    }

    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    setAuthToken(response.data && response.data.data && response?.data?.data?.token)
    return response;
  } catch (e) {
    console.log('Action_LogIn_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const getUserInfo = (data) => async (
  dispatch,
) => {
  console.log('getUserInfo',data)
  dispatch({
    type: USERINFO,
    payload: data,
  });

};
export const verifyOtp = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_verifyOtp_data' + JSON.stringify(data));
    let response = await axios.post(config().verifyOtp, data);

    console.log('Action_verifyOtp_response' + JSON.stringify(response.data.data));
    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    setAuthToken(response.data && response.data.data && response?.data?.data?.token)
    return response;
  } catch (e) {
    console.log('Action_verifyOtp_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};

export const verifyResetOtp = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_verifyOtp_data' + JSON.stringify(data));
    let response = await axios.post(config().verifyResetOtp, data);

    console.log('Action_VerifyOtp_response' + JSON.stringify(response.data.data));
    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    setAuthToken(response.data && response.data.data && response?.data?.data?.token)
    return response;
  } catch (e) {
    console.log('Action_verifyOtp_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const resetPassword = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_resetPassword_data' + JSON.stringify(data));
    let response = await axios.post(config().resetPassword, data);

    console.log('Action_resetPassword_response' + JSON.stringify(response.data.data));
   
    return response;
  } catch (e) {
    console.log('Action_resetPassword_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const resendOtp = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_resendOtp_data' + JSON.stringify(data));
    let response = await axios.post(config().resendOtp, data);

    console.log('Action_vresendOtp_response' + JSON.stringify(response));
   
    return response;
  } catch (e) {
    console.log('Action_vresendOtp_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const fogetPass = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_fogetPass_data' + JSON.stringify(data));
    let response = await axios.post(config().forgotPassword, data);

    console.log('Action_fogetPass_response' + JSON.stringify(response));
   
    return response;
  } catch (e) {
    console.log('Action_fogetPass_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const ResetUserInfo = () => async (dispatch) => {
  dispatch({
    type: USERINFO,
    payload: {},
  }); 
};
export const DeleteAccount = () => async (dispatch) => {
  loadingOn(dispatch);
  try {
    let response = await axios.post(config().deleteAccount);

    console.log('Action_sDeleteAccount_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_DeleteAccount_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};


export const createApplicant = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_createApplicant_data' + JSON.stringify(data));
    let response = await axios.post(config().createApplicant, data);

    console.log('Action_createApplicant_response' + JSON.stringify(response));
   
    return response.data;
  } catch (e) {
    console.log('Action_createApplican_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};