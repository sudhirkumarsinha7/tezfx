export const getUserCollection = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserCollection' + config().userCollectionDetails);
  
      const response = await axios.get(config().userCollectionDetails);
      console.log('Action_getUserCollection_response' + JSON.stringify(response));
      dispatch({
        type: ON_SALE,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserCollection_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };