/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Colors } from "../components/Common/Style";
import { useTheme } from '../theme/Hooks'
import { navigationRef } from './utils'

import TransactionsScreen from '../screens/Transaction/stack';
import HomeScreen from '../screens/home/stack';
import ProfileScreen from '../screens/profile/stack';
import ApiDocsScreen from '../screens/apidocs/stack';

const BottomTabNavigator = ({ navigation, route }) => {
  const Tab = createBottomTabNavigator();
  const { darkMode, NavigationTheme } = useTheme();
  const { colors } = NavigationTheme
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      tabBarOptions={{
        inactiveTintColor: '#DADADA',
        showLabel: true,
        keyboardHidesTabBar: Platform.OS !== 'ios',
        style: {
          backgroundColor: '#fff',
          // paddingBottom:20,
          minHeight: 80,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          switch (route.name) {
            case 'Home':

              return (
                <MaterialIcons
                  style={{ alignSelf: 'center' }}
                  name="home"
                  size={30}
                  color={focused ? 'blue' : '#9999ff'}

                />
              )
            
            case 'Transaction':
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="exchange-alt"
                  size={26}
                  // color={focused ? 'blue' : colors.text}
                  color={focused ? 'blue' : '#9999ff'}


                />)
           
            case 'Profile':
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="user-alt"
                  size={24}
                  color={focused ? 'blue' : '#9999ff'}

                />)
                case 'Developers':
                  return (
                    <SimpleLineIcons
                      style={{ alignSelf: 'center' }}
                      name="docs"
                      size={24}
                      color={focused ? 'blue' : '#9999ff'}
    
                    />)
            default:
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="users"
                  size={26}
                  color={focused ? 'blue' : '#9999ff'}

                />)


          }
        },
        tabBarLabel: ({ focused, color }) => {
          return <Text style={{ color: focused ? 'blue' :'#9999ff', fontSize: 8, fontWeight: 'bold' }}>{((route.name)).toUpperCase()}</Text>
        },
      })}
    >
      <Tab.Screen name="Home" component={HomeScreen} options={{
        headerShown: false
      }} />
     
      <Tab.Screen name="Transaction" component={TransactionsScreen} options={{
        headerShown: false
      }} />
        <Tab.Screen name="Developers" component={ApiDocsScreen} options={{
        headerShown: false
      }} />
     
      <Tab.Screen name="Profile" component={ProfileScreen} options={{
        headerShown: false
      }} />

    </Tab.Navigator>
  );
};
export default BottomTabNavigator;
const styles = StyleSheet.create({});
