import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Login from '../screens/Login';
import InitialScreen from '../screens/InitialScreen';

import SpashScreen from '../SpashScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';

import BottomTabNavigator from './BottomTabNavigator';
const Stack = createNativeStackNavigator();
const headerOptions = {
  headerShown: false
};
const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
      initialRouteName="initial"
    >
      <Stack.Screen name="initial" component={InitialScreen} options={{
        headerShown: false
      }} />
      {/* <Stack.Screen name="Login" component={Login} options={{
        headerShown: false
      }} /> */}
    </Stack.Navigator>
  );
}
export default function MainNavigator() {

  const headerOptions = {
    headerStyle: {
      backgroundColor: '#1a51a2',
    },
    headerTintColor: '#000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    // eslint-disable-next-line sort-keys
    headerLayoutPresent: 'center',
  };
  return (
    <Stack.Navigator screenOptions={headerOptions} headerMode="float">
      <Stack.Screen name='SpashScreen' component={SpashScreen} options={{ headerShown: false }} />
      <Stack.Screen name="initial" component={InitialScreen} options={{
        headerShown: false
      }} />
        <Stack.Screen name='AuthLoading' component={AuthLoadingScreen} options={{ headerShown: false }} />
      <Stack.Screen
        options={{ headerShown: false }}
        name="main"
        component={BottomTabNavigator}
      />
    </Stack.Navigator>
  );
}
